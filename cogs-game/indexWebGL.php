<!DOCTYPE HTML>
<html>
<head>
<title>Canvas test</title>

<script src="js/start-game.js"></script>
<script>
	// use "i" for access to iframe canvasWrapper in console
	// use "j" for access to iframe webglWrapper in console
	// eg. i.JT.$()
	var i = null;
	var j = null;
	window.onload = function() {
		//i = document.getElementById("canvasWrapper").contentWindow;
		j = document.getElementById("webglWrapper").contentWindow;
		//startGame(i,"2d","questionI");
		startGame(j,"webgl","questionJ");
	};
</script>

<style>
body {
	background-color: #cacaca;
}


#webglWrapper {
	width: 1000px;
	height: 512px;
	border: 1px solid #9C9898;
	background-color: white;
	position: relative;
}

.question {
	width: 990px;
	background-color: #dadaaa;
	border: 1px solid #992;
	padding: 5px;
}

#howto {
	background-color: #eee;
	border: 1px solid #999;
	padding: 5px;
}
</style>

</head>
<body>
	<h1 id="header">Demo</h1>
	<a href="index.php">Back to dual mode</a>
	<div id="howto"><b>how to play</b> <br>
	 <ul><li>Read question, find the cog with the same letter as answer to the question and try to connect this one cog with the turning cog (with yellow center) by draging other cogs.<br>
	<li>The powered one cog and these with answers cannot be moved.
	</ul>
	</div>
	<p style="font-size: 70%">NOTE: Actualy there are only 3 levels,
		randomly selected. For access to the iframe in console use variable
		 "j". In "b" are canvas and game objects.</p>
				 WARNING: Internet Explorer does not support WebGL and Opera has some trouble with three.js implementation!
			<div id="questionJ" class="question"></div>
			<iframe id="webglWrapper" src="game.php"></iframe>

	<!-- -->

	<!--
	<div id="canvasWrapper">
	</div>
	<!-- -->

</body>
</html>
