/** JT.CGame.Canvas.js
 * This is part of cogs-game
 *
 * Author: Jan Tulak (jan@tulak.me)
 *
 */

/**
 * @class Represents canvas extended by game methods
 * @augments JT.Canvas
 * 
 * @property {JT.CGame} game - instance of JT.CGame
 * 
 */
JT.CGame.Canvas = function(){
	/** **********************************************************************
	 * private properties
	 ** **********************************************************************/

	/** **********************************************************************
	 * public properties
	 ** **********************************************************************/

	this.game = null;

	this.style.obstacle = {
			color: "#606060",
			backgroundImage: "", 
	};

	this.style.cogs = {
			poweredCogColor: "#FF0",
			nameBgColor : "#8ED6FF",
			nameColor : "#000",
			nameFont : "15px Arial",
			small : {
				texture : null, // normal texture
				highlight : null, // highlight texture
			},
			medium : {
				texture : null, // normal texture
				highlight : null, // highlight texture
			},
			big : {
				texture : null, // normal texture
				highlight : null, // highlight texture
			},
			huge : {
				texture : null, // normal texture
				highlight : null, // highlight texture
			},
			placeholderFill : "#8ED6FF",
			placeholderBorder: "#000000",
			placeholderWidth: 5,
			placeholderAlpha: 0.5,
			
	};

	/** **********************************************************************
	 * private methods
	 ** **********************************************************************/


	/** draw one cog 
	 * @param {object} cog 
	 * @param {number} time - delta time in ms for rotation
	 */
	this._drawCog2d = function(cog,time){
		this._ctx2d.save();

		// if this cog is not fully visible
		if(cog.isVisible() == JT.CGame.Cog.PLACEHOLDER) {
			this._ctx2d.globalAlpha = 0.5;
		} else if(cog.isVisible() == JT.CGame.Cog.INVISIBLE) {
			this._ctx2d.restore();
			return;
		}

		
		var textures = cog.getTextures();
		var x = this.grid.size*cog.getPosition().x;
		var y = this.grid.size*cog.getPosition().y;

		// translate this._ctx2d to center of canvas
		this._ctx2d.translate(x,y);


		// rotate - if speed is not 0, compute new angle
		if(cog.speed != 0){
			//console.log(cog.angle)
			// new angle is old angle + delta
			cog.setAngle(cog.angle + ((time/600)*cog.getSpeed())%360);
		}
		this._ctx2d.rotate(JT.DEGREE*(cog.angle));

		// check for hovering mouse - do not highlight when moving cog
		// or the powered one
		if(cog.isMouseOver(this.game.mouse.x, this.game.mouse.y) && this.game.moving == null && cog.movable && this.game.playable) 
			this._ctx2d.drawImage(textures.highlight, -cog.getFRadius(), -cog.getFRadius(), cog.getFRadius()*2, cog.getFRadius()*2);
		else 
			this._ctx2d.drawImage(textures.normal, -cog.getFRadius(), -cog.getFRadius(), cog.getFRadius()*2, cog.getFRadius()*2);

		this._ctx2d.restore();

		/** if cog is powered */
		if(textures.name){
			// translate this._ctx2d to center of canvas
			this._ctx2d.drawImage(textures.name, x-15, y-15, 30, 30);
		}


		

		//this.drawWithMask(cog.texture, this.images.metal, -cog.getFRadius(), -cog.getFRadius(), cog.getFRadius()*2, cog.getFRadius()*2)

	};
	

	/** draw one cog 
	 * @param {object} cog 
	 * @param {number} time - delta time in ms for rotation
	 */
	this._drawCogWebGL = function(cog,time){
	

		// if this cog is not fully visible
		if(cog.isVisible() == JT.CGame.Cog.PLACEHOLDER) {
			cog.getTextures().webGLNormal.opacity = 0.5;
			
		} else if(cog.isVisible() == JT.CGame.Cog.INVISIBLE) {
			return;
		}else{
			cog.getTextures().webGLNormal.opacity = 1;
		}



		// rotate - if speed is not 0, compute new angle
		if(cog.speed != 0){
			//console.log(cog.angle)
			// new angle is old angle + delta
			cog.setAngle(cog.angle + ((time/600)*cog.getSpeed())%360);
			if(this.game.canvas.webGL instanceof JT.WebGL){
				this.game.canvas.webGL.setCogRot(cog.mesh,cog.angle);
				// test for name texture and rotate in oposite way to keep it stable
				if(cog.mesh.children[0])
				this.game.canvas.webGL.setCogRot(cog.mesh.children[0],-cog.angle);
				
			}

		}
		
		// check for hovering mouse - do not highlight when moving cog
		// or the powered one
		if(cog.isMouseOver(this.game.mouse.x, this.game.mouse.y) && this.game.moving == null && cog.movable && this.game.playable){

			cog.mesh.material = cog.getTextures().webGLHighlight;
		}else {
			if(cog.mesh.material != cog.getTextures().webGLNormal){
				cog.mesh.material = cog.getTextures().webGLNormal;
			}
		}



		//this.drawWithMask(cog.texture, this.images.metal, -cog.getFRadius(), -cog.getFRadius(), cog.getFRadius()*2, cog.getFRadius()*2)

	};

	/** draw placeholder on moving for webGL
	 *      use size in grid steps
	 * @param {number} x
	 * @param {number} y
	 * @param {number} size
	 */
	this._drawPlaceholderWebGL = function(x,y,size){
		this.webGL.placeholders[size].visible = true;
		this.webGL.setCogPos(
				this.webGL.placeholders[size],
				x*this.grid.size,
				y*this.grid.size
			);
		
		return this;
	};
	
	/** draw placeholder on moving
	 *      use size in grid steps
	 * @param {number} x
	 * @param {number} y
	 * @param {number} size
	 */
	this._drawPlaceholder2d = function(x,y,size){

		this._ctx2d.drawImage(
				this.images._placeholders[size],
				x*this.grid.size-size,
				y*this.grid.size-size,
				size*2, size*2
			);

		return this;
	};

	/** draw obstacle 2D
	 * @param {JT.CGame.Obstacle} obstacle
	 */
	this._drawObstacle2d = function (obstacle){
		var o = obstacle.getPositionPx();

		this._ctx2d.save();
		this._ctx2d.beginPath();
		this._ctx2d.rect(
				o.x,
				o.y,
				o.w,
				o.h
		);

		// draw background
		if(typeof this.style.obstacle.backgroundImage == "object" && this.style.obstacle.backgroundImage != null){
			this._ctx2d.fillStyle = this._ctx2d.createPattern(this.style.obstacle.backgroundImage, "repeat");
			//this._ctx2d.fillRect(0,0, this._canvas.width, this._canvas.height);

		}else{
			this._ctx2d.fillStyle = this.style.obstacle.color;
		}
		this._ctx2d.fill();


		this._ctx2d.restore();

	};



	/** **********************************************************************
	 * protected methods
	 ** **********************************************************************/

	/** draw 2D content - this is inserted to drawing method from JT.Canvas
	 * @param {number} time - actual time
	 * @param {number} delta - time from last frame
	 */
	this.drawWebGLContent = function(time,delta){

		// draw all cogs
		for (var i=0;i<this.game.allCogs.length;i++){
			this._drawCogWebGL(this.game.allCogs[i], delta);
		}

		// any cog is moving
		if(this.game.mouse.down && this.game.moving) {
			this._drawPlaceholderWebGL(this.game.mouse.gx, this.game.mouse.gy,this.game.moving.getFRadius());
		}
		
		this.webGL.draw();
		

	};

	/** draw 2D content - this is inserted to drawing method from JT.Canvas
	 * @param {number} time - actual time
	 * @param {number} delta - time from last frame
	 */
	this.draw2DContent = function(time,delta){


		// draw all obstacles
		for (var i=0;i<this.game.obstacles.length;i++){
			this._drawObstacle2d(this.game.obstacles[i], delta);
		}

		// draw all cogs
		for (var i=0;i<this.game.allCogs.length;i++){
			this._drawCog2d(this.game.allCogs[i], delta);
		}

		// any cog is moving
		if(this.game.mouse.down && this.game.moving) {
			this._drawPlaceholder2d(this.game.mouse.gx, this.game.mouse.gy,this.game.moving.getFRadius());
		}

	};

	/** set game for this canvas
	 * @param {JT.CGame} game
	 */
	this.setGame = function(game){
		if(typeof game != "object" && !(game instanceof JT.CGame))
			throw new JT.Exception(JT.E_CODES.BAD_PARAM,"new JT.Canvas.setGame() require JT.CGame as parameter.");
		this.game = game;
		if(game.canvas != this) game.canvas = this;
	};

	/** create image for placeholder
	 * @param {number} r - in px
	 * @rreturns {object} canvas
	 */
	this.createPlaceholder = function(r){
		// create new canvas
		var cvs = this._createCanvas(r*2+this.style.cogs.placeholderWidth,r*2+this.style.cogs.placeholderWidth);
		var ctx = cvs.getContext("2d");
		// draw
		ctx.globalAlpha = this.style.cogs.placeholderAlpha;
		
		ctx.beginPath();
		ctx.arc(
				r+this.style.cogs.placeholderWidth/2,
				r+this.style.cogs.placeholderWidth/2,
				r,
				0, 
				2 * Math.PI, false
			);
		ctx.fillStyle = this.style.cogs.placeholderFill;
		ctx.fill();
		ctx.lineWidth = this.style.cogs.placeholderWidth;
		ctx.strokeStyle = this.style.cogs.placeholderBorder;
		ctx.stroke();
		// create section in images if dont exists
		if(typeof this.images._placeholders == "undefined"){
			this.images._placeholders = {};
		}
		this.images._placeholders[r] = cvs;
	};
	
	

	/** create image for placeholder
	 * @param {string} name
	 * @rreturns {object} canvas
	 */
	this.createNameTexture = function(name,powered){
		// create new canvas
		var cvs = this._createCanvas(30,30);
		var ctx = cvs.getContext("2d");
		
		// draw
		ctx.beginPath();
		ctx.arc(15, 15, 15, 0, 2 * Math.PI, false);
		
		/** if cog is powered select color */
		if(powered){
			ctx.fillStyle = this.style.cogs.poweredCogColor;
		}
		else{
			ctx.fillStyle = this.style.cogs.nameBgColor;
			
		}
		ctx.fill();


		/** if cog has a name, draw it */
		if(name.length){

			ctx.font = this.style.cogs.nameFont;
			ctx.fillStyle = this.style.cogs.nameColor;
			ctx.textAlign = "center";
			ctx.fillText(name,15, 20);
		}
		
		return cvs;
	};
	


	/** Return texture for adequate size.
	 * In case of a bad size, it will return special texture
	 * @param {Number} size
	 * @returns {Object} Image with the right texture
	 */

	this.getTexture = function(size){
		var texture=null;
		switch(size){
		case JT.CGame.Cog.SIZE.SMALL:
			texture=this.style.cogs.small.texture;
			break;
		case JT.CGame.Cog.SIZE.MEDIUM:
			texture=this.style.cogs.medium.texture;
			break;
		case JT.CGame.Cog.SIZE.BIG:
			texture=this.style.cogs.big.texture;
			break;
		case JT.CGame.Cog.SIZE.HUGE:
			texture=this.style.cogs.huge.texture;
			break;
		default:
			texture=this.images.missing;
		break;
		}
		return texture;
	};


	/** Return highlight texture for adequate size.
	 * In case of a bad size, it will return special texture
	 * @param {Number} size
	 * @returns {Object}
	 */

	this.getHighlight = function(size){
		var texture=null;
		switch(size){
		case JT.CGame.Cog.SIZE.SMALL:
			texture=this.style.cogs.small.highlight;
			break;
		case JT.CGame.Cog.SIZE.MEDIUM:
			texture=this.style.cogs.medium.highlight;
			break;
		case JT.CGame.Cog.SIZE.BIG:
			texture=this.style.cogs.big.highlight;
			break;
		case JT.CGame.Cog.SIZE.HUGE:
			texture=this.style.cogs.huge.highlight;
			break;
		default:
			texture=this.images.missing;
		break;
		}
		return texture;
	};



	/** **********************************************************************
	 * constructor
	 ** **********************************************************************/


	/**************/
	return this;
};
JT.CGame.Canvas.inheritsFrom(JT.Canvas);