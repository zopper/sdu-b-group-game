/** start-game.js
 * This is part of Cogs-game
 *
 * Author: Jan Tulak (jan@tulak.me)
 * 
 */

/** Prepare and run game 
 * @param {object} dom - iframe content in which it should run
 * @param {string} type - 2d or webgl
 * @param {string} qName - ID of html element for question
 * */
function startGame(dom, type, qName){
	var game = new dom.JT.CGame();
	var canvas = new dom.JT.CGame.Canvas();
	this.c = canvas;
	this.g = game;


//	set images for loading
	var sources = {
			// these are black-transparent masks
			cog256: "images/cog_256b.png",
			cog128: "images/cog_128b.png",
			cog64: "images/cog_64b.png",
			cog32: "images/cog_32b.png",

			// other images
			missing: "images/smile.png",
			metal: "images/metal.jpg",
			metal2: "images/metal-texture_small.jpg",
			wood: "images/wood2.jpg",
	};

	/** manualy created maps */

	var maps = new Array();
	var answers = new Array();
	var rightAnswer = "A";

	maps.push(function(){ // map 0
		console.log(type+" map 0");
		// set question
		document.getElementById(qName).innerHTML = '5 + 3 = ?<br>\n';
		document.getElementById(qName).innerHTML += 'A) 8';
		rightAnswer = "A";


		game.addObstacle(4,1,4,1);
		
		//create the huge powered cog
		game.addCog(-4,8,dom.JT.CGame.Cog.SIZE.HUGE,5);
		// create the next cog
		game.addCog(6,8,dom.JT.CGame.Cog.SIZE.MEDIUM); 


		// create these small cogs        
		game.addCog(13,8,dom.JT.CGame.Cog.SIZE.SMALL);
		game.addCog(11,10,dom.JT.CGame.Cog.SIZE.SMALL);
		game.addCog(11,6,dom.JT.CGame.Cog.SIZE.SMALL);
		game.addCog(11,8,dom.JT.CGame.Cog.SIZE.SMALL);
		game.addCog(9,8,dom.JT.CGame.Cog.SIZE.SMALL);

		// then the medium sized
		game.addCog(11,3,dom.JT.CGame.Cog.SIZE.MEDIUM);
		answers.push(game.addCog(11,15,dom.JT.CGame.Cog.SIZE.MEDIUM));
		game.addCog(16,8,dom.JT.CGame.Cog.SIZE.MEDIUM);

		// and the one big on right side
		game.addCog(22,8,dom.JT.CGame.Cog.SIZE.BIG);

	});
	maps.push(function(){ // map 1
		console.log(type+" map 1");
		// set question
		document.getElementById(qName).innerHTML = '5 - 3 = ?<br>\n';
		document.getElementById(qName).innerHTML += 'A) 8<br>\n';
		document.getElementById(qName).innerHTML += 'B) 2<br>\n';
		document.getElementById(qName).innerHTML += 'C) 5<br>\n';
		rightAnswer = "B";


		game.addObstacle(4,1,4,1);
		
		//create the huge powered cog
		game.addCog(12,6,dom.JT.CGame.Cog.SIZE.MEDIUM,35);

		game.addCog(12,3,dom.JT.CGame.Cog.SIZE.SMALL);
		game.addCog(12,1,dom.JT.CGame.Cog.SIZE.SMALL);

		// set cogs for answers
		answers.push(game.addCog(18,6,dom.JT.CGame.Cog.SIZE.MEDIUM));
		answers.push(game.addCog(6,6,dom.JT.CGame.Cog.SIZE.MEDIUM));
		answers.push(game.addCog(12,12,dom.JT.CGame.Cog.SIZE.MEDIUM));

	});
	maps.push(function(){ // map 2
		console.log(type+" map 2");
		// set question
		document.getElementById(qName).innerHTML = 'm*c^2 = ?<br>\n';
		document.getElementById(qName).innerHTML += 'A) U<br>\n';
		document.getElementById(qName).innerHTML += 'B) X<br>\n';
		document.getElementById(qName).innerHTML += 'C) E<br>\n';
		rightAnswer = "C";

		// obstacle looks like L
		game.addObstacle(14,5,5,1);
		game.addObstacle(18,5,1,5);

		game.addCog(16,8,dom.JT.CGame.Cog.SIZE.MEDIUM,50);

		// set cogs for answers
		answers.push(game.addCog(16,2,dom.JT.CGame.Cog.SIZE.MEDIUM));
		answers.push(game.addCog(22,8,dom.JT.CGame.Cog.SIZE.MEDIUM));
		answers.push(game.addCog(2,12,dom.JT.CGame.Cog.SIZE.BIG));

		game.addCog(11,8,dom.JT.CGame.Cog.SIZE.SMALL);
		game.addCog(9,8,dom.JT.CGame.Cog.SIZE.SMALL);
		game.addCog(2,2,dom.JT.CGame.Cog.SIZE.MEDIUM);
		game.addCog(6,2,dom.JT.CGame.Cog.SIZE.MEDIUM);

	});



//	generate random previous map and actual map
	var lastMap = Math.floor(Math.random()*100)% maps.length;
	var random = Math.floor(Math.random()*100)% maps.length;

	/** prepare next level */
	function startLevel(){
		game.clean();
		answers = new Array();
		canvas.hideBigText(false,function(){
			game.playable=true;
		});

		// while random map is the same as previous, generate new number
		while(random == lastMap)
			random = Math.floor(Math.random()*100)% maps.length;

		maps[random ]();
		//maps[1 ]();
		lastMap = random;
		// generate right and bad answers position
		var length = answers.length;
		for(var n=0;n<length;n++){
			// if this 
			var r = Math.floor(Math.random()*100)% answers.length;

			if(rightAnswer == String.fromCharCode(65+n)){
				game.setCogAsRight(answers[r],String.fromCharCode(65+n));
				answers.splice(r,1);
			}else{
				game.setCogAsBad(answers[r],String.fromCharCode(65+n));
				answers.splice(r,1);
			}
		}

	}

	game.setCanvas(canvas);

	canvas.setContainer(dom.JT.$("body"));
	canvas.setGrid(32,true);

	canvas.loadImages(sources,function(images){
		// create textures with masks
		canvas.images.c32 = canvas.createMask(canvas.images.cog32,canvas.images.metal);
		canvas.images.c64 = canvas.createMask(canvas.images.cog64,canvas.images.metal);
		canvas.images.c128 = canvas.createMask(canvas.images.cog128,canvas.images.metal);
		canvas.images.c256 = canvas.createMask(canvas.images.cog256,canvas.images.metal);

		// set backgrounds
		//canvas.style.obstacle.backgroundImage = canvas.images.metal2;
		//canvas.style.backgroundImage = canvas.images.wood;

		// set textures
		canvas.style.cogs.small.texture = canvas.images.cog32;
		canvas.style.cogs.medium.texture = canvas.images.cog64;
		canvas.style.cogs.big.texture = canvas.images.cog128;
		canvas.style.cogs.huge.texture = canvas.images.cog256;
		// set highlight textures
		canvas.style.cogs.small.highlight = canvas.images.c32;
		canvas.style.cogs.medium.highlight = canvas.images.c64;
		canvas.style.cogs.big.highlight = canvas.images.c128;
		canvas.style.cogs.huge.highlight = canvas.images.c256;

		// select 2d canvas or webgl
		canvas.setContext(type);
		//canvas.setContext("webgl");


		// set what to do after answers
		// startLevel() will be called again and again on succes
		game.setSuccess(true,function(){startLevel();});
		game.setFail(false);

		// start level
		startLevel();

		canvas.run();
		game.run();

	});
 return this;
}




