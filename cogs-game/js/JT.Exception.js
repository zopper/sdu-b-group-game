/** JT.Exception.js
 * This is part of cogs-game
 *
 * Author: Jan Tulak (jan@tulak.me)
 * 
 */


/** 
 * @class ENUM of error codes
 * 
 * @param {number} OK
 * @param {number} BAD_PARAM
 * @param {number} NO_PLACE
 * @param {number} CANT_SET
 * @param {number} CANT_MOVE
 */
JT.E_CODES = {
		OK : true,
		BAD_PARAM : -1,
		NO_PLACE : -2,
		CANT_SET : -3,
		CANT_MOVE : -4,
		NO_PLACE 	: -5,
		
};


/** 
 * @class Represents my exception
 * 
 * @param code <E_CODE>
 * @param {string} msg
 * @param data - could be whatever for next usage
 */
JT.Exception = function (code,msg,data){
	this.code = code;
    this.message = msg;
    this.data = data;
    
    this.toString = function(){
            return "Exception (code: "+this.code+") - "+this.message;
    };
    
    
};
JT.Exception.prototype=new JT.Exception;