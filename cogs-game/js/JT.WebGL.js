/** JT.webGL.js
 * This is part of cogs-game
 *
 * Author: Jan Tulak (jan@tulak.me)
 * 
 */


/** 
 * @class Represents canvas extended by project-specific methods
 * @Require three.js
 * 
 * @param {object} canvas - which object called
 * 
 * @property {object} renderer - THREE.WebGLRenderer
 * @property {object} camera - THREE.PerspectiveCamera
 * @property {object} scene - THREE.Scene
 * @property {object} placeholders
 * @property {object} activeCog
 * @property {object} activePlaceholder
 * 
 */
JT.WebGL = function(canvas){
	/** **********************************************************************
	 * private properties
	 ** **********************************************************************/



	this._renderer = null;

	/** **********************************************************************
	 * public properties
	 ** **********************************************************************/
	this.camera = null;
	this.scene = null;
	this.placeholders = {};
	// last time modified cog/placeholder for fast reset
	this.activeCog = null;
	this.activePlaceholder = null;
	// big text items
	this.bigTextBG = null;
	this.bigTextStringTex = null;
	this.bigTextString = null;
	/** **********************************************************************
	 * private methods
	 ** **********************************************************************/

	/** **********************************************************************
	 * protected methods
	 ** **********************************************************************/
	/** convert string color to hex color
	 * @param {string} str
	 * 
	 * @returns {number}
	 */
	this.color2hex = function(str){
		var s;
		if(str.length == 4){
			s=str+str.slice(1,str.length);
		}else{
			s=str;
		}
		return parseInt(s.slice(1,s.length),16);
	};


	/** create mesh for cog and place it to scene
	 * 
	 * @param {number} r
	 * @param {object} normal - image/canvas with texture
	 * @param {object} highlight - image/canvas with texture
	 * 
	 * @returns {object} mesh
	 */
	this.createCog = function(r,normal,highlight){

		var texture = {map: new THREE.Texture(normal),transparent: true,};
		texture.map.needsUpdate = true;
		var materialNormal = new THREE.MeshBasicMaterial(texture);

		texture = {map: new THREE.Texture(highlight),transparent: true,};
		texture.map.needsUpdate = true;
		var materialHighlight = new THREE.MeshBasicMaterial(texture);

		var plane = new THREE.Mesh(new THREE.PlaneGeometry(r*2,r*2), materialNormal);
		plane.overdraw = true;

		this.scene.add(plane);

		// create placeholder for this size, if do not exist
		if(typeof this.placeholders[r] == "undefined"){
			// create texture
			var tex = {
					transparent: true,
					map: new THREE.Texture(canvas.images._placeholders[r]),
			};
			tex.map.needsUpdate = true;
			var pMaterial = new THREE.MeshBasicMaterial(tex);
			this.placeholders[r]  =  new THREE.Mesh(new THREE.PlaneGeometry(r*2,r*2), pMaterial);
			this.placeholders[r].position.z = 10;
			this.placeholders[r].visible = false;
			// add to scene
			this.scene.add(this.placeholders[r]);
		}

		// return result
		return {
			mesh:plane,
			texture:materialNormal,
			highlight:materialHighlight
		};
	};


	/** create mesh for cog name, add it to cog and place it to scene
	 * 
	 * @param {object} cog mesh
	 * @param {object} image - canvas or img element
	 * @returns {object} mesh
	 */
	this.createCogName = function(cog, image){

		var texture = {
				map: new THREE.Texture(image),
				transparent: true,
		};
		texture.map.needsUpdate = true;
		var material = new THREE.MeshBasicMaterial(texture);

		var plane = new THREE.Mesh(new THREE.PlaneGeometry(30,30), material);
		plane.position.z = 5;
		plane.overdraw = true;

		cog.add(plane);


		// return result
		return plane;
	};




	/** move cog to position
	 * @param {objectt} o
	 * @param {number} x
	 * @param {number} y
	 * 
	 */
	this.setCogPos = function(o,x,y){
		o.position.x = x;
		o.position.y = -y;
	};


	/** set cog rotation
	 * @param {object} o
	 * @param {number} angle
	 */
	this.setCogRot = function (o,angle){
		o.rotation.z = -angle*JT.DEGREE;
	};

	/** create mesh for obstacle and place it to scene
	 * 
	 * @param {number} x
	 * @param {number} y
	 * @param {number} width
	 * @param {number} height
	 * 
	 * @returns {object} mesh
	 */
	this.createObstacle = function(){
		// material
		var texture = null;
		if(typeof canvas.style.obstacle.backgroundImage == "object" && canvas.style.obstacle.backgroundImage != null){

			texture = {map: new THREE.Texture(canvas.style.obstacle.backgroundImage)};

			texture.map.needsUpdate = true;
		}else{
			texture = {color: this.color2hex(canvas.style.obstacle.color) };
			//texture = new THREE.Texture(canvas.images.cog32);
		}
		// texture = THREE.ImageUtils.loadTexture(this.images.cog128.src)
		var material = new THREE.MeshBasicMaterial(texture);
		var plane = new THREE.Mesh(new THREE.PlaneGeometry(1,1), material);
		plane.overdraw = true;

		this.scene.add(plane);
		return plane;
	};

	/** set position and size of an obstacle
	 * @param {object} o - obstacle
	 * @param {number} x
	 * @param {number} y
	 * @param {number} w
	 * @param {number} h
	 */
	this.setObstaclePos  = function(o,x,y,w,h){
		o.position.x = x+w/2;
		o.position.y = -y-h/2;
		o.scale.x = w;
		o.scale.y = h;
	};



	this._drawBG = function(){
		/** DEBUG */
		// material
		//var texture = THREE.ImageUtils.loadTexture(this.images.cog128.src)
		var material = null;
		if(canvas.style.backgroundImage){
			var texture = new THREE.Texture(canvas.style.backgroundImage);
			texture.needsUpdate = true;
			material = new THREE.MeshBasicMaterial({ map: texture });
		}else{
			material = new THREE.MeshBasicMaterial({color: this.color2hex(canvas.style.backgroundColor)});
		}


		var plane = new THREE.Mesh(new THREE.PlaneGeometry(canvas.getSize().x, canvas.getSize().y), material);

		plane.position.z = -100;
		plane.position.x = canvas.getSize().x/2;
		plane.position.y = -canvas.getSize().y/2;

		plane.overdraw = true;
		this.scene.add(plane);

		// draw grid if displayed
		if(canvas.grid.display){
			if(typeof canvas.images._grid == "undefined") {
				canvas.createGrid();
			}
			var textureGrid = new THREE.Texture(canvas.images._grid);
			textureGrid.needsUpdate = true;
			var materialGrid = new THREE.MeshBasicMaterial({ map: textureGrid,transparent:true });
			var planeGrid = new THREE.Mesh(new THREE.PlaneGeometry(canvas.getSize().x, canvas.getSize().y), materialGrid);

			planeGrid.position.z = -90;
			planeGrid.position.x = canvas.getSize().x/2;
			planeGrid.position.y = -canvas.getSize().y/2;

			planeGrid.overdraw = true;
			this.scene.add(planeGrid);
		}
	};


	/** create plane for big text background
	 * @param {string} color
	 */
	this.createBigTextBG = function(color){

		var material = new THREE.MeshBasicMaterial({color: this.color2hex(color), transparent:true});
		material.opacity = 0;
		var plane = new THREE.Mesh(new THREE.PlaneGeometry(canvas.getSize().x, canvas.getSize().y), material);

		plane.position.z = 100;
		plane.position.x = canvas.getSize().x/2;
		plane.position.y = -canvas.getSize().y/2;

		plane.overdraw = true;
		this.bigTextBG = plane;
		this.scene.add(plane);

	};

	/** create plane for big text string
	 * @param {string} img - texture with text
	 */
	this.createBigTextString = function(img){
		this.bigTextStringTex = new THREE.Texture(img);
		this.bigTextStringTex.needsUpdate = true;
		var material = new THREE.MeshBasicMaterial({map: this.bigTextStringTex, transparent:true});
		material.opacity = 0;
		var plane = new THREE.Mesh(new THREE.PlaneGeometry(canvas.getSize().x, canvas.getSize().y), material);

		plane.position.z = 101;
		plane.position.x = canvas.getSize().x/2;
		plane.position.y = -canvas.getSize().y/2;

		plane.overdraw = true;
		this.bigTextString = plane;
		this.scene.add(plane);

	};




	/** set camera
	 * @param {object} position - x, y, z
	 * @param {object} rotation - x, y, z
	 */
	this.setCamera = function(position, rotation){
		if( typeof position != "object" || typeof rotation != "object")
			throw new JT.Exception(JT.E_CODES.BAD_PARAM, "JT.WebGL.setCamera() require objects as parameters");




		this.camera = new THREE.OrthographicCamera(0, window.innerWidth ,0, -window.innerHeight, - 2000, 2000 );


		if(typeof position.x == "number") this.camera.position.x = position.x;
		if(typeof position.y == "number") this.camera.position.y = position.y;
		if(typeof position.z == "number") this.camera.position.z = position.z;

		if(typeof rotation.x == "number") this.camera.rotation.x = rotation.x;
		if(typeof rotation.y == "number") this.camera.rotation.y = rotation.y;
		if(typeof rotation.z == "number") this.camera.rotation.z = rotation.z;

		this.scene.add(this.camera);
		return this.camera;
	};

	/** return the canvas object (eg. JT.Canvas)
	 * @return {object}
	 */
	this.getParent = function(){
		return canvas;
	};

	/** draw one frame
	 * @params {time} time - time of frame
	 * 
	 */
	this.draw = function(){
		//this.renderer.render(this.scene, this.camera);
		this._renderer.render(this.scene, this.camera);
	};


	/** recompute X position from "0 on left side" to "0 in middle"
	 * @public
	 * 
	 * @param {number} x
	 */
	this.x = function(x){
		return (x*canvas.grid.size );
	};
	/** recompute Y position from "0 on the top" to "0 in middle"
	 * @public
	 * 
	 * @param {number} y
	 */
	this.y = function(y){
		return (-y*canvas.grid.size );
	};

	/** clear the scene
	 * - remove obstacles and cogs, but left the camera, backgrounds...
	 * 
	 */
	this.clean = function(){
		// remove obstacles
		for (var i=canvas.game.obstacles.length; i>0;i--){
				this.scene.remove(canvas.game.obstacles[i-1].mesh);
		}
		// remove cogs
		for (var i=canvas.game.allCogs.length; i>0;i--){
				this.scene.remove(canvas.game.allCogs[i-1].mesh);
		}

	};


	/** **********************************************************************
	 * constructor
	 ** **********************************************************************/
	this._renderer = new THREE.WebGLRenderer();
	this._renderer.setSize(canvas.getSize().x, canvas.getSize().y);
	canvas.getContext().ctxWebGL.appendChild(this._renderer.domElement);

	// scene
	this.scene = new THREE.Scene();
	// camera
	/* this.camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 1000);
    this.camera.position.y = -450;
    this.camera.position.z = 400;
    this.camera.rotation.x = 45 * (Math.PI / 180);
	 */
	this.setCamera({
		x : 0,
		y : 0,
		z : 618
	},{
		x : 0 * (Math.PI / 180),
		y : undefined,
		z : undefined
	});

	;
	//this.createObstacle(3,3,10,10);
	this._drawBG();

	/*******/
	return this;
};
JT.WebGL.inheritsFrom(JT.Class);

