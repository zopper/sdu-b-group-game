/** JT.CGame.Obstacle.js
 * This is part of Cogs-game
 *
 * Author: Jan Tulak (jan@tulak.me)
 * 
 */

/** 
 * @class Implements one obstacle
 * 
 * @param {JT.CGame} game
 * 
 * @property {object} _position
 * @property {object} _drawSize
 * @property {number} padding - static
 * @property {object} style
 * @property {object} mesh
 */
JT.CGame.Obstacle = function(game){
	/** **********************************************************************
	 * private properties
	 ** **********************************************************************/
	
	this._position = {
			x:0,
			y:0,
			w:0,
			h:0,
	};
	this._drawSize = {
			x:0,
			y:0,
			w:0,
			h:0,
	};
	
	
	/** **********************************************************************
	 * public properties
	 ** **********************************************************************/
	this.mesh = null;
	// static
	JT.CGame.Obstacle.padding =5;
	
	/** **********************************************************************
	 * private methods
	 ** **********************************************************************/

	/** recompute drawing size for new size/padding
	 * 
	 */
	this._recomputeSize = function(){


		this._drawSize.w = game.canvas.grid.size*this._position.w - 2*JT.CGame.Obstacle.padding;
		this._drawSize.x = game.canvas.grid.size*this._position.x + JT.CGame.Obstacle.padding;

		this._drawSize.h = game.canvas.grid.size*this._position.h - 2*JT.CGame.Obstacle.padding;
		this._drawSize.y = game.canvas.grid.size*this._position.y + JT.CGame.Obstacle.padding;
		

		// set webgl model if accessible
		if(game.canvas.webGL instanceof JT.WebGL){
			game.canvas.webGL.setObstaclePos(this.mesh,
					this._drawSize.x,
					this._drawSize.y,
					this._drawSize.w,
					this._drawSize.h);
			
		}

	};
	/** **********************************************************************
	 * protected methods
	 ** **********************************************************************/

	/** return position and size in pixels
	 * @returns {object}
	 */
	this.getPositionPx = function(){
		return {
			x:this._drawSize.x,
			y:this._drawSize.y,
			w:this._drawSize.w,
			h:this._drawSize.h,
			};
	};
	
	
	/** set padding
	 * @param {number} padding
	 */
	this.setPadding = function(padding){
		this._padding = padding;
		this._recomputeSize();
	};
	
	/** set begining of this obstacle
	 * @public
	 * 
	 * @param {number} x
	 * @param {number} y
	 * 
	 * @returns this
	 */
	this.setStart = function(x,y){
		this._position.x = x;
		this._position.y = y;
		this._recomputeSize();
		return this;
	};

	/** set size of this obstacle
	 * @public
	 * 
	 * @param {number} width
	 * @param {number} height
	 * 
	 * @returns this
	 */
	this.setSize = function(width,height){
		if(width < 0 || height < 0) 
			throw new JT.Exception(JT.E_CODES.BAD_PARAM, 
					"JT.CGame.Obstacle.setSize() can't accept negative values!");
		this._position.w = width;
		this._position.h = height;
		this._recomputeSize();
		return this;
	};
	
	/** test if cog with given position and radius is inside of this obstacle
	 * Returns true if yes
	 * @public
	 * 
	 * @param {number} x - in grid steps
	 * @param {number} y
	 * @param {number} r - radius
	 * 
	 * @returns {boolean} 
	 */
	this.testCollision = function(x,y,r){
		
		/** left upper corner */
		var A = {x:this._position.x, y:this._position.y};
		/** right upper corner */
		var B = {x : this._position.x + this._position.w, y : this._position.y};
		/** right bottom corner */
		var C = {x : this._position.x + this._position.w, y : this._position.y + this._position.h};
		/** left bottom corner */
		var D = {x : this._position.x, y : this._position.y + this._position.h};

		/** at first test if middle of the cog is inside of this obstacle */
		if( (x >= A.x && x <= C.x ) && (y >= A.y && y <= C.y )){
			return true;
		}
		
		/** At second test distance between middle of cog and corners of obstacle
		 * This distance must be greater then radius otherwise the cog has collision
		 */
		if(r > game.distance(x,y,A.x,A.y) || r > game.distance(x,y,B.x,B.y) ||
				r > game.distance(x,y,C.x,C.y) || r > game.distance(x,y,D.x,D.y)
		){
			return true;
		}
		
		/** maybe the corners are ok, but now we check distance from sides
		 * At first find which two sides will be checked.
		 * If the X point is between X values two sides, then use another two sides
		 * and if the Y point is between Y values, then use the first two sides.�
		 * (cannot be between all sides, this is tested in the very first test)
		 * 
		 * When sides are selected, then compare their Y (or X) value with cog position and 
		 * radius.
		 */
		if( x > A.x && x < C.x ){
			if(game.distance(A.x,A.y,A.x,y) < r || game.distance(C.x,C.y,C.x,y) < r)
				return true;
		}else if(y > A.y && y < C.y ){
			if(game.distance(A.x,A.y,x,A.y) < r || game.distance(C.x,C.y,x,C.y) < r)
				return true;
		}
		
		return false;
	};
	
	/** **********************************************************************
	 * constructor
	 ** **********************************************************************/

	/** set arguments to variables */
	if (typeof game == "object" && game instanceof JT.CGame){ 
		this._game = game; 
	}
	
	return this;
};
JT.CGame.Obstacle.inheritsFrom(JT.Class);