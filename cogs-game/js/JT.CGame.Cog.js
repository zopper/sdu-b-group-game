/** JT.CGame.Cog.js
 * This is part of cogs-game
 *
 * Author: Jan Tulak (jan@tulak.me)
 * 
 */

/** 
 * @class new class
 * 
 * @param {JT.CGame} game
 * @param {number} givenRadius - radius in grid steps as 
 * 
 * @property {object} _position
 * @property {number} _radius Radius in px
 * @property {number} _gRadius Radius in steps of grid
 * @property {number} _teeth
 * @property {number} _t180 - half of t360 - angle of only one tooth, without the space
 * @property {number} _t360 - angle between two teeth
 * @property {object} _textures - highlight and normal texture 
 * @property {JT.CGame} _game - pointer to instance of JT.CGame
 * @property {number} _visibility - JT.CGame.Cog.VISIBLE/PLACEHOLDER/INVISIBLE
 * @property {number} _teethHeight
 * @property {number} _speed
 * @property {string} _name
 * 
 * @property {boolean} movable
 * @property {object} mesh
 * @property {number} angle
 * @property {JT.CGame.Cog} parent
 * @property {array} children - array of JT.CGame.Cog
 * @property {number} VISIBLE constant
 * @property {number} PLACEHOLDER constant
 * @property {number} INVISIBLE constant
 * @property {number} SIZE constant object
 */
JT.CGame.Cog = function(game, givenRadius){
	/** **********************************************************************
	 * private properties
	 ** **********************************************************************/
	this._position = {x:0,y:0};
	this._radius = 0;
	this._radiusFull = 0;
	this._gRadius = 0;
	this._teeth = 0;
	this._t180 = 0; 
	this._t360 = 0; 
	this._textures = {normal:null, highlight:null,webGLNormal:null,webGLHighlight:null,name:null};
	this._visibility = JT.CGame.Cog.VISIBLE;
	this._game = null;
	this._teethHeight = 20; // height of teeth in pixels
	this._speed = 0; 
	this._name = "";

	/** **********************************************************************
	 * public properties
	 ** **********************************************************************/
	this.movable = true;
	this.mesh = null;
	this.angle = 0;
	this.parent = null;
	this.children = new JT.CGame.CogsArray();
	/** **********************************************************************
	 * private methods
	 ** **********************************************************************/

	/** add new child to this cog
	 * DO NOT CALL EXTERNALY 
	 * @param {Cog_c} child
	 */
	this._addChild = function(child){
		this.children.push(child);
	};
	/** remove a child from this cog
	 *  DO NOT CALL EXTERNALY
	 *  @param {Cog_c} child
	 */
	this._removeChild = function(child){
		this.children.splice(this.children.indexOf(child));
	};
	
	/** **********************************************************************
	 * protected methods
	 ** **********************************************************************/

	/** get used texture
	 * @public
	 * 
	 * @return {object} {normal, highlight}
	 */
	this.getTextures = function(texture){
		return this._textures;

	};
	/** get name
	 * @public
	 * 
	 * @returns {string}
	 */
	this.getName = function(){
		return this._name;
	};

	/** get speed
	 * @public
	 * 
	 * @returns {number}
	 */
	this.getSpeed = function(){
		return this._speed;
	};


	/** get radius
	 * @public
	 * 
	 * @returns {number}
	 */
	this.getRadius = function(){
		return this._radius;
	};

	/** get full radius
	 * @public
	 * 
	 * @returns {number}
	 */
	this.getFRadius = function(){
		return this._radiusFull;
	};

	/** get gRadius
	 * @public
	 * 
	 * @returns {number}
	 */
	this.getGRadius = function(){
		return this._gRadius;
	};

	/** get teeth
	 * @public
	 * 
	 * @returns {number}
	 */
	this.getTeeth = function(){
		return this._teeth;
	};

	/** get t360
	 * @public
	 * 
	 * @returns {number}
	 */
	this.getT360 = function(){
		return this._t360;
	};

	/** get t180
	 * @public
	 * 
	 * @returns {number}
	 */
	this.getT180 = function(){
		return this._t180;
	};


	/** get position
	 * @public
	 * 
	 * @returns {object} - {x,y}
	 */
	this.getPosition = function(){
		return this._position;
	};

	/** show this cog
	 * @public
	 * 
	 */
	this.show = function(){
		this._visibility = JT.CGame.Cog.VISIBLE;
		return this;
	};
	/** hide this cog
	 * @public
	 * 
	 */
	this.hide = function(){
		this._visibility = JT.CGame.Cog.PLACEHOLDER;
		return this;
	};


	/** is this cog visible? 
	 * @public
	 * 
	 * @returns {bool}
	 */
	this.isVisible = function(){
		return this._visibility;
	};

	/** test if given point is on this cog
	 * @public
	 * 
	 * @returns {bool}
	 */
	this.isMouseOver = function(x,y){
		var delta = this._radius*0.8;
		var cX = this._position.x*this._game.canvas.grid.size;
		var cY = this._position.y*this._game.canvas.grid.size;
		// console.log(cX+ " : "+cY+ ", delta: "+delta)
		if( (x > cX-delta && x < cX+delta) 
				&& (y > cY-delta && y < cY+delta)){
			return true;

		}
		return false;
	};

	/** set game for this cog
	 * @public
	 * 
	 * @param {JT.CGame} game
	 */
	this.setGame = function(game){
		if(typeof game != "object" || !(game instanceof JT.CGame) )
			throw JT.Exception(JT.E_CODES.BAD_PARAMS,"JT.CGame.Cog.setGame(game) require instance of JT.CGame as parameter");
		this._game = game;
	};

	/** set speed of rotation
	 * If no argument is given, compute speed from parent.
	 * Note: Manually can be set only for one wheel!
	 * @public
	 * 
	 * 
	 * @param {number} speed - approx. in degrees/sec
	 */
	this.setSpeed = function(speed){
		if(typeof speed == "number"){
			this._speed = speed;
			this.movable = false;
		}else{
			var pSpeed = (this.parent.getSpeed())* this.parent.getRadius();
			this._speed = -pSpeed/(this._radius);
		}

		return this;
	};


	/** set angle
	 * If no argument is given, compute angle from parent.
	 * Note: Usualy there is not reason for external calling!
	 * @public
	 * 
	 * @param {number} [angle] - in degrees
	 * 
	 * Note2: Getting this to properly working state took me 2 days, 
	 * 6 black candles, 1 black rooster, 1 white mouse, some voo-doo figures
	 * and many f-words.
	 */
	this.setAngle = function(angle){

		if(typeof angle == "number")
			this.angle = angle;
		else{
			/* // this is longer version without pre-computed constants
                    this.actualAngle = (180/this._teeth)  
                            -(this.parent._teeth/this._teeth) 
			 *  (this.parent.actualAngle % (360/this.parent._teeth)) ;
			 */
			// compute initial angle
			var offset = this._t180 
			- this.parent.getTeeth()/this._teeth
			*  (this.parent.angle % (this.parent.getT360())) ;

			/* Decide if round up or down - it depends on the side
			 * the cog is turning.
			 * The unfinished tooth must be canceled, so round always
			 * to zero ( 10.9 => 10, -10.9 => -10).
			 * Math.floor/ceil are always rounding down/up 
			 * (floor: -1.2 => -2, 1.2 => 1)
			 * 
			 * After this condition, in newAngle will be angle without
			 * the unfinished tooth-turn
			 */
			var newAngle=0;
			if(this._speed<0){
				newAngle = this._t360*Math.ceil(this.angle/this._t360);
			}else{
				newAngle = this._t360*Math.floor(this.angle/this._t360);
			}
			// if this is the small cog then a wild hack appears!
			// (too small cog for right teeth placement)
			if(this._gRadius == JT.CGame.Cog.SIZE.SMALL){
				// if parent is on same Y value, then do nothing
				// but if it is on same X value (and is not same size), add offset
				if(this.parent.getGRadius() != JT.CGame.Cog.SIZE.SMALL && this.parent.getPosition().x == this._position.x)
					newAngle += this._t180;
			}
			// and if the parent is small cog, then it also could happens
			else if(this.parent.getGRadius() == JT.CGame.Cog.SIZE.SMALL){
				// if parent is on same Y value, then do nothing
				// but if it is on same X value (and is not same size), add offset
				if( this.parent.getPosition().x == this._position.x)
					newAngle += this._t180;
			}

			/** dunno why, but in one half of tooth it is turning to
			 * towards by one tooth and in the second not. Also sometimes when rebuilding tree
			 * there can be jump over one tooth.
			 * This condition make test for it (on 3 decimal places) and if the old angle
			 * and the new are different only in number of turned teet, will do nothing.
			 */
			if(Math.round(this.angle*1000) != Math.round((newAngle+offset-this._t360)*1000)){
				this.angle=newAngle+offset;
			}
		}
		
		return this;
	};


	/** set used texture
	 * @public
	 * 
	 * @param {image} texture
	 */
	this.setTexture = function(texture){
		this._textures.normal=texture;
		return this;
	};
	/** set highlight texture
	 * @public
	 * 
	 * @param {image} texture
	 */
	this.setHighlight = function(texture){
		this._textures.highlight=texture;
		return this;
	};


	/** set used texture
	 * @public
	 * 
	 * @param {image} texture
	 */
	this.setWebGLTexture = function(texture){
		this._textures.webGLNormal=texture;
		return this;
	};
	/** set highlight texture
	 * @public
	 * 
	 * @param {image} texture
	 */
	this.setWebGLHighlight = function(texture){
		this._textures.webGLHighlight=texture;
		return this;
	};
	

	/** set name
	 * @public
	 * 
	 * @param {string} name
	 */
	this.setName = function(name){
		this._name=name;
		return this;
	};


	/** set nameTexture
	 * @public
	 * 
	 * @param {object} tex
	 */
	this.setNameTexture = function(tex){
		this._textures.name=tex;
		return this;
	};
	
	/** set parent of this cog
	 * To unset parent, give null as parameter
	 * @public
	 * 
	 * @param {Cog_c|null} parent
	 */
	this.setParent= function(parent){
		// if parent is actually set, unset it first
		if(this.parent) this.parent._removeChild(this);
		// add this cog as child to the new parent
		if(parent) parent._addChild(this);
		// set new parent
		this.parent = parent;

		/* set new speed and initial angle */
		if(this.parent){

			// set speed
			this.setSpeed();

			// and angle
			this.setAngle();


		}else{
			// no parent? then no speed
			this._speed = 0;
			//this.initAngle = 0;
		}
		return this;
	};

	/** detach this cog from scene and stop all cogs whos are
	 * powered by this one
	 * actualy do nothing
	 * @public
	 * 
	 */
	this.detach = function () {

		return this;
	};


	/** Attach this cog to scene and start all cogs whos are now
	 * powered by this one
	 */
	this.attach = function () {
		this._game.rebuildTree();
		return this;
	};

	/** check if given cog is next to this
	 * Return true if yes
	 * @public
	 * 
	 * @param {Cog_c} cog
	 * @returns {bool}
	 */
	this.isNext = function(cog){
		return (
				this._game.distance(this._position.x, this._position.y,cog.getPosition().x,cog.getPosition().y) 
				== 
					(cog.getGRadius()+this._gRadius)
		);
	};

	/** Try move this cog to given position.
	 * @public
	 * 
	 * @throw {JT.Exception} if cannot be moved.
	 * @param {number} x
	 * @param {number} y
	 * 
	 */
	this.moveTo = function(x,y){

		// test for space
		if(!this._game.testSpace(this,x, y))
			throw new JT.Exception(JT.E_CODES.NO_PLACE,
					"There is no place on "+x+":"+y+
					" (r="+this._gRadius+").");

		// there is space so move it
		this._position.x = x;
		this._position.y = y;


		


		return this;
	};




	/** **********************************************************************
	 * constructor
	 ** **********************************************************************/


	/** set arguments to variables */
	if (typeof game == "object" && game instanceof JT.CGame){ 
		this._game = game; 
	}
	else throw "JT.CGame.Cog() require instance of JT.CGame as first parameter!";

	if (typeof givenRadius == "number"){
		this._gRadius = givenRadius; 
		this._radiusFull = givenRadius*this._game.canvas.grid.size+this._teethHeight/4; 
		this._radius = givenRadius*this._game.canvas.grid.size;
		this._gRadius = givenRadius;
		switch(givenRadius){
		case JT.CGame.Cog.SIZE.SMALL:
			this._teeth = 10; 
			break;
		case JT.CGame.Cog.SIZE.MEDIUM:
			this._teeth = 20; 
			break;
		case JT.CGame.Cog.SIZE.BIG:
			this._teeth = 40; 
			break;
		case JT.CGame.Cog.SIZE.HUGE:
			this._teeth = 80; 
			break;
		}
		this._t180 = 180/this._teeth;
		this._t360 = 360/this._teeth;
	}
	else throw "JT.CGame.Cog() require number as second parameter!";



	return this;
};
JT.CGame.Cog.inheritsFrom(JT.Class);


/** **********************************************************************
 * constant properties and methods
 ** **********************************************************************/
JT.CGame.Cog.VISIBLE = 1;
JT.CGame.Cog.PLACEHOLDER = 2;
JT.CGame.Cog.INVISIBLE = 3;
JT.CGame.Cog.SIZE = {
		SMALL : 1,
		MEDIUM :2,
		BIG : 4,
		HUGE : 8
};
