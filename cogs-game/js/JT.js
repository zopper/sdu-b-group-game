/** JT.js
 * This is part of cogs-game
 *
 * Author: Jan Tulak (jan@tulak.me)
 * 
 * Suggested files to use with this namespace:
 * JT.Exception.js 	- custom exceptions 
 */

/** **********************************************************************
 *  NAMESPACE
 ** **********************************************************************/
if(typeof JT == "object") throw "Error: Cannot create JT namespace, some object with same name already exists!";

/** 
 * @namespace All classes related to cogs-game
 */
var JT = function(){
	REVISION = "1";
};
JT.DEGREE = (Math.PI/180);
/** **********************************************************************
 *  CLASS
 ** **********************************************************************/
/** 
 * @class Represents generic class
 */
JT.Class = function(){
	return this;
};

Function.prototype.inheritsFrom = function( parentClassOrObject ){ 
	if ( parentClassOrObject.constructor == Function ) 
	{ 
		//Normal Inheritance 
		this.prototype = new parentClassOrObject;
		this.prototype.constructor = this;
		this.prototype.parent = parentClassOrObject.prototype;
	} 
	else 
	{ 
		//Pure Virtual Inheritance 
		this.prototype = parentClassOrObject;
		this.prototype.constructor = this;
		this.prototype.parent = parentClassOrObject;
	} 
	return this;
} ;


/** **********************************************************************
 *  DOM elements and nodes testing
 ** **********************************************************************/

/** checking if object is an DOM node
 * @author http://stackoverflow.com/questions/384286/javascript-isdom-how-do-you-check-if-a-javascript-object-is-a-dom-object
 * 
 * @param {object} o
 * 
 * @returns true if it is a DOM node
 */
JT.isNode = function(o){
	return (
			typeof Node === "object" ? o instanceof Node : 
				o && typeof o === "object" && typeof o.nodeType === "number" && typeof o.nodeName==="string"
	);
};
/** checking if object is an DOM element
 * @author http://stackoverflow.com/questions/384286/javascript-isdom-how-do-you-check-if-a-javascript-object-is-a-dom-object
 * 
 * @param {object} o
 * 
 * @returns true if it is a DOM element
 */ 
JT.isElement = function (o){
	return (
			typeof HTMLElement === "object" ? o instanceof HTMLElement : //DOM2
				o && typeof o === "object" && o.nodeType === 1 && typeof o.nodeName==="string"
	);
};


/** **********************************************************************
 *  HTHML elements selector
 ** **********************************************************************/

/** Easier access to HTML elements - jQuery like
 * @param {string} str - identifier of element
 */
JT.$ = function (str){
	if(typeof str != "string") throw JT.Exception(JT.E_CODES.BAD_PARAM, "JT.$(string) require string!");

	var result=new Array();
	/**
	 * split string into tag, id, class and selectors
	 */

	var tag="";
	var cls="";
	var id="";
	//var selectors = "";
	var ar = str.match(/^([^\.#\[]+)?(\.[^#\[]+)?(#[^\[]+)?(\[[^#\[]+\])?/);
	tag = ar[1];
	cls = ar[2];
	if(cls)cls=cls.slice(1); // remove "."
	id = ar[3];
	if(id) id = id.slice(1); // remove "#"
	//selector = ar[4].slice(1,-1);



	/*
	console.log("tagEnd: "+tagEnd+", dotPos: "+dotPos+", classEnd: "+classEnd+", hashPos: "+hashPos+", selPos: "+selPos);
	console.log("tag: "+tag);
	console.log("class: "+cls);
	console.log("id: "+id);
	console.log("selectors: "+selectors);
	 */	

	/** search for tag */
	if(tag && tag.length > 0){
		result = document.getElementsByTagName(tag);
	}

	/** search for class */
	if(cls && cls.length > 0){
		var prev = new Array();
		for(var i = 0; i<result.length;i++)prev.push(result[i]);

		var clss = document.getElementsByClassName(cls);

		/** if there was some searching before */
		if(prev && prev.length>0){
			// prepare for cross-searching
			result = new Array();
			/** do cross-search */
			for(var pc=0;pc<prev.length;pc++){
				for(var i=0;i<clss.length;i++){
					if(prev[pc] === clss[i]){
						result.push(prev[pc]);
					}
				}
			}
		}else{
			result = clss;
		}
	}

	/** search for id */
	if(id && id.length > 0){
		var prev = new Array();
		for(var i = 0; i<result.length;i++)prev.push(result[i]);

		var ids = document.getElementById(id);
		/** if there was some searching before */
		if(prev && prev.length>0 && ids !=null){
			// prepare for cross-searching
			/** do cross-search */
			for(var pc=0;pc<prev.length;pc++){
				if(prev[pc] === ids){
					result = new Array(ids);
				}

			}
		}else {
			if(ids != null)
				result = new Array(ids);
			else
				result = new Array();
		}
	}

	/** search for selectors */
	//var selectors;
	// TODO selectors

	/** return result */
	if(result.length == 1){
		return result[0];
	}else if (result.length == 0){
		return null;
	}
	return result;
};
