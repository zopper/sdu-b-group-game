/** JT.canvas.js
 * This is part of cogs-game
 *
 * Author: Jan Tulak (jan@tulak.me)
 *
 */

/**
 * @class Represents canvas extended by my own usefull methods
 * @Require JT.WebGL for webgl drawing
 * 
 * @property {object} _container - DOM element
 * @property {object} _size - {x,y}
 * @property {object} _canvas - DOM element
 * @property {object} _ctxWebGL - DOM element
 * @property {object} _ctx2d - context of _canvas
 * @property {object} _bigText - stored informations about big text
 * @property {object} _animation - stored informations about computing frames - FPS, time of last frame..
 * 
 * @property {object} images - list of textures and other images
 * @property {JT.WebGL} webGL - instance of JT.WebGL 
 * @property {object} grid - {size,display}
 * @property {object} style - visual style configuration - colors, fonts...
 * @property {JT.CGame} game - instance of JT.CGame
 * @property {string} canvasID - ID of _canvas element
 * @property {string} webGLID - ID of _ctxWebGL element
 * 
 */

// FIXME flickering after fade-out for "this is bad"
JT.Canvas = function(){
	/** **********************************************************************
	 * private properties
	 ** **********************************************************************/
	this._container = null;

	this._size = {x:0,y:0};

	this._canvas = null; // canvas element 
	this._ctx2d = null; // 2D context
	this._ctxWebGL = null; // element for webgl drawing

	this._bigText = {
			text : "",
			timeEnd : 0, 
			fadeIn : 0,
			fadeOut : 0,
			timeStart : 0,
			color : "white", // default color text
			alpha : 0,
			callback : null,
			dontRemove : false
	};


	this._animation = {
			lastTime : 0,
			// FPS ---------
			showFPS : false,
			// The higher this value, the less the FPS will be affected by quick changes
			// Setting this to 1 will show you the FPS of the last sampled frame only
			filter : 50,
			max: 0, // max time per frame
			min: 1/0, // max time per frame
			fps : 0,
			toCount : 10, // skip first X frames before counting
			stats : null, // testing purpose

	};
	/** **********************************************************************
	 * public properties
	 ** **********************************************************************/
	this.images = {};
	this.webGL = null; // JT.WebGL instance
	this.grid = {
			/** default of grid */
			size: 32,
			/** will be grid displayed? */
			display: false,
	};
	
	this.style = {
			/** background of screen - color*/
			backgroundColor:"#fff",
			/** background of screen - image */
			backgroundImage: null, 
			/** properties for dimmed screen */
			dimScreen : {
					/** color of dimmed screen */
					color:"#101010",
					/** alpha of dimmed screen */
					alpha: 0.8,
					fadeDuration: 250, // fade-in/out duration in ms
					font: "40pt Arial",
			},
			grid : {
				/** normal grid color */
				colorNormal : "#ddd",
				/** color for every n-th line */
				colorHighlight : "#888",
				/** what N is the "every n-th line"? */
				highlight : 4,
				/** line width */
				lineWidth : 1,
			},
				
			
	};

	this.canvasID = null;
	this.webGLID = null ;

	/** **********************************************************************
	 * private methods
	 ** **********************************************************************/

	/** initialize animation frame
	 * @return this
	 */
	this._initAnim = function(){
		// set animation timer
		window.requestAnimFrame = (function(callback) {
			return window.requestAnimationFrame ||
			window.webkitRequestAnimationFrame ||
			window.mozRequestAnimationFrame ||
			window.oRequestAnimationFrame ||
			window.msRequestAnimationFrame ||
			function(callback) {
				window.setTimeout(callback, 1000 / 60);
			};
		})();
	};


	/** will only place text with FPS
	 * 
	 */
	this._showFPS = function(){
		this._ctx2d.fillText("FPS: "+Math.round(this._animation.fps)+", lowest FPS: "+this._animation.min+", max time per frame: "+this._animation.max+" ms.", 10, 10);
	};
	/** create canvas element
	 *  If no size is given, use size of container
	 * @param {number} [x] - size
	 * @param {number} [y] - size
	 * @return {object} canvas DOM element
	 */
	this._createCanvas = function(x,y){
		// create new canvas
		var el  = document.createElement("canvas");
		el.setAttribute("height", typeof y == "number"? (y+"px") : (this._size.y+"px"));
		el.setAttribute("width", typeof x == "number" ? (x+"px") : (this._size.x+"px"));
		var ctx = el.getContext("2d");
		ctx.clearRect(-el.width, -el.height, 2*el.width, 2*el.height);
		return el;
	};
	/** create webgl drawing element
	 *  If no size is given, use size of container
	 * @return {object} div DOM element
	 */
	this._createWebGLDrawing = function(){
		// create new canvas
		var el = document.createElement("div");
		return el;
	};


	/** compute one animation frame
	 * 
	 */
	this._draw = function(){
		//console.log("anim")

		// delta between this and last frame
		var time = new Date().getTime();
		var delta = time - this._animation.lastTime;
		//animation.lastTime = time;

		/** ******************************************
		 *  BEGIN FPS
		 ** ******************************************/
		if(this._animation.toCount == 0){
			var thisFrameFPS = 1000 / delta;
			var newFPS = this._animation.fps+(thisFrameFPS - this._animation.fps) / this._animation.filter;
			if(newFPS)this._animation.fps = newFPS;

			if(this._animation.min > thisFrameFPS && thisFrameFPS >= 1){
				this._animation.min = Math.round(thisFrameFPS);
			};
		}
		this._animation.lastTime = time;
		// this.showFPS();
		/** ******************************************
		 *  END FPS
		 ** ******************************************/

		/** ******************************************
		 *  BEGIN DRAWING
		 ** ******************************************/

		if(this.webGL instanceof JT.WebGL){
			this.drawWebGLContent(time, delta);
			
			this._drawBigText(time);
			
			this.webGL.stats.update();
		}
		if(this._ctx2d){
			// clear canvas
			this._ctx2d.clearRect(0, 0, this._canvas.width, this._canvas.height);
			
			// draw background
			if(typeof this.style.backgroundImage == "object" && this.style.backgroundImage != null){
				this._ctx2d.fillStyle = this._ctx2d.createPattern(this.style.backgroundImage, "repeat");
				this._ctx2d.fillRect(0,0, this._canvas.width, this._canvas.height);
				
			}
			
			//console.log("should be cleaned")
			// draw grid
			this._drawGrid();

			this.draw2DContent(time, delta);
			
			this._drawBigText(time);

			this._animation.stats.update();
			//this._showFPS();

		}

		/** ******************************************
		 *  END DRAWING
		 ** ******************************************/

		// show fps if it should be visible
		if(this._animation.showFPS) showFPS();

		// request new frame
		//if(!this.runAnimation) return;
		var p = this;
		requestAnimFrame(function() {
			p._draw();
		});


		/** DETECTING MAX TIME PER FRAME
		 */
		var end = (new Date().getTime()-time);
		if(this._animation.max < end && this._animation.toCount == 0)
			this._animation.max = end;
		if(this._animation.toCount>0) this._animation.toCount--;

	};



	/** Draw grid
	 * In case of disabled grid will draw nothing.
	 * On first run it will create new canvas with same size as the main canvas
	 * and draw the grid into it. Then and on others runs will only draw the
	 * image.
	 * @return this
	 */
	this._drawGrid = function(){
		if(!this.grid.display) return;

		// if grid is undefined, then this function is running for first time
		// so create image with grid and save it
		if(typeof this.images._grid == "undefined") {

			this.createGrid();
		}
		// grid image is existing - draw it
		this._ctx2d.drawImage(this.images._grid, 0, 0);


	};
	

	/** create text for big text
	 */
	this._createBigTextString = function(){
		if(typeof this.images._bigTextString == "undefined"){
			this.images._bigTextString = this._createCanvas(this._size.x, this._size.y);
			if(this.webGL){
				this.webGL.createBigTextString(this.images._bigTextString);
			}
		}
		var cvs = this.images._bigTextString;
		var ctx = cvs.getContext("2d");
		/** write text */
		ctx.clearRect(0, 0, this._size.x, this._size.y);
		ctx.font = this.style.dimScreen.font;
		ctx.fillStyle = this._bigText.color;
		ctx.textAlign = "center";
		ctx.fillText(this._bigText.text, this._size.x/2, this._size.y/2);
		 // if webgl is active, then set it 
		if(this.webGL){
			this.webGL.bigTextStringTex.needsUpdate = true;
		}
	};
	
	/** create background for big text
	 */
	this._createBigTextBG = function(){

		 // if webgl is active, then set it 
		if(this.webGL){
			this.webGL.createBigTextBG(this.style.dimScreen.color,this.style.dimScreen.alpha);
		}else{
			
			this.images._bigTextBG = this._createCanvas(this._size.x, this._size.y);
	
			var cvs = this.images._bigTextBG;
			var ctx = cvs.getContext("2d");
			/** write text */
			ctx.globalAlpha = this.style.dimScreen.alpha;
			ctx.beginPath();
			ctx.rect(0,0, this._size.x, this._size.y);
			ctx.fillStyle = this.style.dimScreen.color;
			ctx.fill();
		}
	};

	/** draw big text over screen
	 * @param {number} time - actual time in ms 
	 */
	this._drawBigText = function(time){
		// if there is something set
		if(this._bigText.text.length){
			// if the text sould be visible
			if(this._bigText.timeEnd > time ||this._bigText.dontRemove){
				
				// compute alpha value for animation
				if (this._bigText.fadeIn > 0){
					/** we are at the start, so make fade-in
					 * alpha is computed as how_many_time_from_this_animDuration_left/animDuration
					 */
					this._bigText.fadeIn -= time-this._bigText.timeStart;
					this._bigText.alpha =  (this.style.dimScreen.fadeDuration-this._bigText.fadeIn)/this.style.dimScreen.fadeDuration;

				} else if(!this._bigText.dontRemove && this._bigText.fadeOut > 0 && time > this._bigText.timeEnd-this.style.dimScreen.fadeDuration){
					/** if we are at the end, make fade-out
					 * alpha is computed as how_many_time_from_this_animDuration_left/animDuration
					 */
					this._bigText.fadeOut -= time-this._bigText.timeStart;
					this._bigText.alpha = this._bigText.fadeOut/this.style.dimScreen.fadeDuration;

				}else  if(this._bigText.timeEnd < time){
					/** the timer ended, but the text should stay - so can call callback
					 */
					if(typeof this._bigText.callback == "function"){
						/** use temp variable and empty the _bigText.callback before calling
						 * because callback can set new text with new callback
						 */
						var c = this._bigText.callback;
						this._bigText.callback = null;
						c();
					}
				}
				this._bigText.timeStart = time;

				// draw it
				if(this.webGL){
					this.webGL.bigTextBG.material.opacity = this.style.dimScreen.alpha*this._bigText.alpha;
					this.webGL.bigTextString.material.opacity = this._bigText.alpha;
				} else if(this._ctx2d){
					this._ctx2d.save();

					/** create background */
					this._ctx2d.globalAlpha = this._bigText.alpha;
					this._ctx2d.drawImage(this.images._bigTextBG, 0, 0, this._size.x, this._size.y);
					this._ctx2d.restore();

					/** write text */
					this._ctx2d.save();
					this._ctx2d.globalAlpha = this._bigText.alpha;
					this._ctx2d.drawImage(this.images._bigTextString, 0, 0, this._size.x, this._size.y);
					this._ctx2d.restore();
				}
				
				
			}else{ // if timeEnd > time - else 
				
				/** else delete it (if can) */
				if(!this._bigText.dontRemove) this._bigText.text="";
				// and call callback
				if(typeof this._bigText.callback == "function"){
					/** use temp variable and empty the _bigText.callback before calling
					 * because callback can set new text with new callback
					 */
					var cb = this._bigText.callback;
					this._bigText.callback = null;
					cb();
				}

			} // if timeEnd > time - else 
		} // if text.length
	};
	
	
	/** create overlay for big text
	 * 
	 */

	/** will set font style
	 * @param {string} font
	 */
	this._setFont = function (font){
		if(this._ctx2d) this._ctx2d.font = font;
	};

	/** **********************************************************************
	 * protected methods
	 ** **********************************************************************/

	/** set event listener to context which is accessible
	 * same syntax as domElement.addEventListener()
	 * @param {string} type
	 * @param {function} listener
	 * @param {boolean} [useCapture]
	 */
	this.addEventListener = function(type, listener, useCapture){
		if(this._canvas) this._canvas.addEventListener(type, listener, useCapture);
		else if(this._ctxWebGL) this._ctxWebGL.addEventListener(type, listener, useCapture);
	};
	
	/** set showing big text over screen - like "YOU HAVE FAILED"
	 * @param {string} text
	 * @param {number} duration - in seconds
	 * @param {string} color
	 * @param {function} callback - is called after the text fade-off
	 * @param {boolean} [dontRemove] - if true, the text will be not removed automaticaly
	 */
	this.setBigText = function(text, duration, color, callback, dontRemove){
		this._bigText.color = color;
		this._bigText.dontRemove = dontRemove;
		this._bigText.callback = callback;

		if(this._bigText.text == ""){
			// there is no showed text - count with animations
			this._bigText.fadeIn = this.style.dimScreen.fadeDuration;
			// set time for hidding the text - if the text should not be hidden, then dont count with fade-of animation
			if(dontRemove){
				this._bigText.fadeOut = 0;
				this._bigText.timeEnd = new Date().getTime() + 1000*duration + this.style.dimScreen.fadeDuration;
			}else{
				this._bigText.fadeOut = this.style.dimScreen.fadeDuration;
				this._bigText.timeEnd = new Date().getTime() + 1000*duration + 2*this.style.dimScreen.fadeDuration;
			}
		}else{
			// there is currently text set, so skip fade-on animation
			this._bigText.fadeIn  =0;
			// set time for hidding the text - if the text should not be hidden, then dont count with fade-of animation
			if(dontRemove){
				this._bigText.fadeOut = 0;
				this._bigText.timeEnd = new Date().getTime() + 1000*duration;
			}else{
				this._bigText.fadeOut = this.style.dimScreen.fadeDuration;
				this._bigText.timeEnd = new Date().getTime() + 1000*duration + this.style.dimScreen.fadeDuration;
			}
		}
		this._bigText.timeStart = new Date().getTime();
		this._bigText.text = text;
		this._createBigTextString();

	};
	
	/** hide the big text
	 * note: will skip old callback
	 * note: if no bigText is displayed, will do nothing
	 * @public
	 * 
	 * @param {boolean} immediately
	 * @param {function} [callback]
	 */
	this.hideBigText = function(immediately,callback){
		if(this._bigText.text == "") return;
		
		this._bigText.callback = callback;
		this._bigText.dontRemove = false;
		if(immediately) {
			this._bigText.timeEnd = 0;
			this._bigText.text = "";
		}else{
			this._bigText.fadeOut = this.style.dimScreen.fadeDuration;
			this._bigText.timeEnd = new Date().getTime()+this.style.dimScreen.fadeDuration;
		}
	};



	/** images loader
	 * @author http://www.html5canvastutorials.com/
	 * 
	 * @param {object} sources - list of images to load {name:"path"}
	 * @param {function} callback - will be called after all images will be loaded
	 */
	this.loadImages = function(sources, callback) {
		//var images = {};
		if(typeof this.images != "object") this.images = {};
		var loadedImages = 0;
		// get num of sources
		var numImages = Object.keys(sources).length;
		// load images
		for(var src in sources) {
			this.images[src] = new Image();
			this.images[src].onload = function() {
				if(++loadedImages >= numImages) {
					callback(this.images);
				}
			};
			this.images[src].src = sources[src];
		}
	};



	/** set images objects after their loading
	 * @param {object} img
	 */
	this.setImages = function (img){
		this.images = img;
		return this;
	};


	/** set grid
	 * @method
	 * @param {number} size  - size of grid step in pixels
	 * @param {boolean} show - optional, if true, show grid
	 */
	this.setGrid = function (size, show){
		if (typeof size == "number")
			this.grid.size = size;
		else
			throw new JT.Exception(JT.E_CODES.BAD_PARAM, "setGrid(size, show) require a number as 1st argument!");
		
		if (typeof show == "boolean" || typeof show == "undefined"){
			if(show){
				this.grid.display = true;
			}
		}else{
			throw new JT.Exception(JT.E_CODES.BAD_PARAM, "setGrid(size, show) require a boolean as 2nd argument!");
		}
		return this;
	};


	/** set container for canvas/webgl
	 * @param {object} cont - html element
	 *
	 * @returns this
	 */

	this.setContainer = function (cont){
		// test for data type
		if (typeof cont != "object" || !JT.isElement(cont) )
			throw new JT.Exception(JT.E_CODES.BAD_PARAM, "setContainer(cont) require an HTML element as argument!");
		this._container = cont;
		// if container is body, then use window size
		if(cont === JT.$("body")){
			this._size.x = window.innerWidth;
			this._size.y = window.innerHeight;
		}else{
			this._size.x = cont.clientWidth;
			this._size.y = cont.clientHeight;
		}
		return this;
	};

	/** set canvas 2D element
	 * @param {object} cvs - canvas HTML element
	 *
	 * @returns this
	 */
	this.setCanvas = function (cvs){
		// test for data type
		if (typeof cvs != "object" || !JT.isElement(cvs) || cvs.tagName.toLowerCase() != "canvas")
			throw new JT.Exception(JT.E_CODES.BAD_PARAM, "setCanvas(canvas) require an canvas HTML element as argument!");
		this._canvas = cvs;
		return this;
	};

	/** set context
	 * @param {string} type - what type
	 */
	this.setContext = function(type) {
		switch(type){
		/** create 2D context */
		case "2d":
			/** create new element */
			var ctx = this._createCanvas();
			ctx.setAttribute("class","screen");
			ctx.style.position = "absolute";
			ctx.style.top="0px";
			ctx.style.left="0px";
			if(this.style.backgroundColor.length) ctx.style.backgroundColor = this.style.backgroundColor;
			this.canvasID = "JT.Canvas.Canvas."+Math.random();
			ctx.setAttribute("id",this.canvasID);
			this._container.appendChild(ctx);
			this.setCanvas(ctx);
			/** set context */
			this._ctx2d = this._canvas.getContext("2d");
			
			

			// stats

			this._animation.stats = new Stats();
			this._animation.stats.domElement.style.position = 'absolute';
			this._animation.stats.domElement.style.top = '0px';
			this._animation.stats.domElement.style.zIndex = 100;
			this._container.appendChild( this._animation.stats.domElement );
			
			break; 

			/** create webgl context */
		case "webgl":
			/** create new element */
			var ctx = this._createWebGLDrawing();
			this._container.appendChild(ctx);
			this._ctxWebGL = ctx;
			this.webGL =	new JT.WebGL(this);
			/** save ID and size to newly created canvas */
			this.webGLID = "JT.Canvas.WebGL."+Math.random();
			ctx.children[0].setAttribute("id",this.webGLID);
			if(this.style.backgroundColor.length) ctx.children[0].style.backgroundColor = this.style.backgroundColor;
			ctx.children[0].setAttribute("class","screen");

			// stats

			this.webGL.stats = new Stats();
			this.webGL.stats.domElement.style.position = 'absolute';
			this.webGL.stats.domElement.style.top = '0px';
			this.webGL.stats.domElement.style.zIndex = 100;
			this._container.appendChild( this.webGL.stats.domElement );

			break;

			/** create both context */
			/*
		case "both":
			this.setContext("2d");
			this.setContext("webgl");

			break;*/
			default:
				throw new JT.Exception(JT.E_CODES.BAD_PARAM,"JT.Canvas.setContext() - context can be only 2d or webgl!");
				break;
		}
	};
	

	/** Will apply mask on image. Returned image will has size of mask 
	 * (if not given explicit size) 
	 * and will be opaque only where mask is.
	 * @param {image} mask  
	 * @param {image} image - masked image
	 * @param {number} width - width of result
	 * @param {number} height - height of result
	 * 
	 * @returns {object} canvas element - can be act as image
	 */
	this.createMask = function(mask,image,width,height){
		var w = mask.width;
		var h = mask.height;
		if(typeof width == "number"){
			w = width;
		}
		if(typeof height == "number"){
			h = height;
		}
		// create new canvas
		var cvs = this._createCanvas(w,h);
		var ctx = cvs.getContext("2d");

		// clear it
		ctx.save();

		//draw the mask
		ctx.globalCompositeOperation = "copy";
		ctx.drawImage(mask, 0, 0, w, h);
		//draw the image to be masked, but only where both it
		//and the mask are opaque; see http://www.whatwg.org/specs/web-apps/current-work/multipage/the-canvas-element.html#compositing for details.
		ctx.globalCompositeOperation = "source-in";
		ctx.drawImage(image, 0, 0);
		ctx.restore();

		return cvs;
	};


	/** create grid
	 * In case of disabled grid will draw nothing.
	 * 
	 * @return this
	 */
	this.createGrid = function(){
		if(!this.grid.display) return;


			// create new canvas
			var cvs = this._createCanvas();
			cvs.removeAttribute("id");
			var ctx = cvs.getContext("2d");

			// clear it
			ctx.clearRect(0, 0, cvs.width, cvs.height);
			// draw vertical lines
			for(var i=0; i < this._size.x; i = i+this.grid.size){
				ctx.beginPath();
				ctx.moveTo(i, 0);
				ctx.lineTo(i, this._size.y);
				ctx.lineWidth = this.style.grid.lineWidth;
				ctx.strokeStyle = this.style.grid.colorNormal;
				// every n-th line draw with another color
				if(i%(this.style.grid.highlight*this.grid.size)==0)
					ctx.strokeStyle = this.style.grid.colorHighlight;
				ctx.stroke();
			}
			// draw horizontal lines
			for(i=0; i < this._size.y; i = i+this.grid.size){
				ctx.beginPath();
				ctx.moveTo(0, i);
				ctx.lineTo(this._size.x,i);
				ctx.lineWidth = this.style.grid.lineWidth;
				ctx.strokeStyle = this.style.grid.colorNormal;
				// every n-th line draw with another color
				if(i%(this.style.grid.highlight*this.grid.size)==0)
					ctx.strokeStyle = this.style.grid.colorHighlight;
				ctx.stroke();

			}
			this.images._grid = cvs;
		
	};


	/** get canvas
	 * @returns {object} html element of canvas
	 */
	this.getCanvas = function(){
		return this._canvas;
	};

	/** get default context
	 * @returns {object} {ctx2d, ctxWebGL}
	 */
	this.getContext = function(){
		return {ctx2d: this._ctx2d, ctxWebGL: this._ctxWebGL};
	};

	/** return size of canvas
	 * @return {object} {x,y}
	 */
	this.getSize = function(){
		return {x:this._size.x,y:this._size.y};
	};


	/** get mouse coordinates on canvas
	 * @author http://www.html5canvastutorials.com/
	 * 
	 * @param {object} canvas - DOM element
	 * @param {object} evt - event 
	 * 
	 */
	this.getMousePos = function(canvas, evt) {
		var mouse = new Object();
		if(this._canvas){
			var rect = canvas.getBoundingClientRect(), root = document.documentElement;
			// return relative mouse position
			mouse.x = evt.clientX - rect.left - root.scrollLeft;
			mouse.y = evt.clientY - rect.top - root.scrollTop;
		}else if(this._ctxWebGL){
			mouse.x = evt.clientX;
			mouse.y = evt.clientY;
		}
		return mouse;
	};

	/** start rendering
	 * @return this
	 */
	this.run = function(){

		this._createBigTextBG();

		this._initAnim();

		// this is because FPS is using default font settings
		this._setFont( "9pt Arial");
		this._draw();
	};

	
	/** draw 2D content - will be replaced in extending class
	 * @param {number} time - actual time
	 * @param {number} delta - time from last frame
	 */
	this.draw2DContent = function(time,delta){
		
	};
	

	/** draw WebGL content - will be replaced in extending class
	 * @param {number} time - actual time
	 * @param {number} delta - time from last frame
	 */
	this.drawWebGLContent = function(time,delta){
		
	};

	/** **********************************************************************
	 * constructor
	 ** **********************************************************************/


	/**************/
	return this;
};
JT.Canvas.inheritsFrom(JT.Class);