/** JT.CGame.js
 * This is part of Cogs-game
 *
 * Author: Jan Tulak (jan@tulak.me)
 * 
 */




/** 
 * @class new class
 * 
 * USAGE:
 * 	window.onload = function() {
 * 		// set images for loading
 *   	var sources = {
 *			cog256: "images/cog_256b.png",
 *		}
 *		game = new JT.CGame();
 *		canvas = new JT.CGame.Canvas();
 *		game.setCanvas(canvas);
 *       
 * 		canvas.setContainer(JT.$("body"));
 *		canvas.setGrid(32,true);
 *       
 *		canvas.loadImages(sources,function(images){
 *			// create textures with masks
 *			canvas.images.c32 = canvas.createMask(canvas.images.cog32,canvas.images.metal);
 *
 *			// set backgrounds
 *			canvas.style.backgroundImage = canvas.images.wood;
 *		
 *			// set textures
 *			canvas.style.cogs.small.texture = canvas.images.cog32;
 *			// set highlight textures
 *			canvas.style.cogs.small.highlight = canvas.images.c32;
 *		
 *			// set context
 *			canvas.setContext("2d");
 *			
 *			// add obstacles
 *			game.addObstacle(4,1,4,2)
 *			//create the powered cog
 *			game.addCog(-4,8,JT.CGame.Cog.SIZE.HUGE,5);
 *			// create another cogs
 *			var A = game.addCog(24,8,JT.CGame.Cog.SIZE.BIG);
 *           ...
 *			// set answers
 *			game.setCogAsRight(A,"A");
 *			game.setCogAsBad(B,"B");
 *
 *			// set what to do after answers
 *			game.setSuccess(true,function(){alert("WIN")});
 *			game.setFail(false);
 *			// run
 *			canvas.run();
 *			game.run();
 *		}; // end of loadImages
 *	}; // end of window.onload
 *
 * 
 * Obstacles SHOULD be placed BEFORE cogs. 
 * There is no collision checking while placing obstacles, so any obstacle 
 * can be placed into existing cog, while cogs are checking for collision and 
 * in an attempt to place it on obstacle raise an exception!
 * 
 * 
 * @require JT.CGame.Cog
 * @require JT.CGame.Obstacle
 * 
 * @property {function} _successCallback - called after user answer right
 * @property {function} _failCallback - called after user answer bad
 * @property {boolean} _stopOnSuccess - should game end on right answer?
 * @property {boolean} _stopOnFail - should game end on bad answer?
 * 
 * @property {JT.Canvas} canvas - instance of JT.Canvas
 * @property {JT.CGame.CogsArray} allCogs
 * @property {JT.CGame.CogsArray} movableCogs
 * @property {JT.CGame.Cog} poweredCog
 * @property {JT.CGame.CogsArray} rightCogs - cogs which means win
 * @property {JT.CGame.CogsArray} badCogs - cogs which means loose
 * @property {JT.CGame.obstaclesArray} obstacles
 * @property {object} mouse - mouse informations {x,y,gx,gy,down}
 * @property {object} lastPos - last save position of last moved cog for return in case of bad move {x,y}
 * @property {JT.CGame.Cog} lastMoved - last moved cog
 * @property {JT.CGame.Cog} moving - pointer to cog which is actualy moving by user
 * @property {boolean} playable - can user interact?
*/
JT.CGame = function(cvs){
	/** **********************************************************************
	 * private properties
	 ** **********************************************************************/

	/** callback for results */
	this._successCallback = null;
	this._stopOnSuccess = false;
	this._failCallback = null;
	this._stopOnFail = false;
	/** **********************************************************************
	 * public properties
	 ** **********************************************************************/
	this.canvas = null;
	
	this.allCogs = new JT.CGame.CogsArray();
	this.movableCogs = new JT.CGame.CogsArray();
	this.poweredCog = null;
	this.rightCogs = new JT.CGame.CogsArray();
	this.badCogs = new JT.CGame.CogsArray();

	this.obstacles = new JT.CGame.ObstaclesArray();

	this.mouse = {
			// cursor position in px
			x: 0, 
			y: 0, 
			// nearest grid point for cursor
			gx: 0,
			gy: 0,
			// is pressed button?
			down: false,
			// where was button pressed
			xDown: 0,
			yDown: 0
	};
	this.moving = null;
	this.playable = false;
	this.lastMoved = null;
	this.lastPos = {x:0,y:0};
	

	/** **********************************************************************
	 * private methods
	 ** **********************************************************************/

	/** called when is powered one of the rights cogs
	 * 
	 */
	this._success = function(){

		this.playable = false;

		var p = this;
		this.canvas.setBigText("This is CORRECT!", 2, "green", function(){
			//p.playable = true;
			p.canvas.setBigText("The end?", 2, "white", function(){
				p.playable = !p._stopOnSuccess;
				if(typeof p._successCallback == "function") p._successCallback();
			},p._stopOnSuccess);
		},true);

	};

	/** called when is powered one of the bads cogs
	 * 
	 */
	this._failure = function(){

		this.playable = false;
		var p = this;
		this.canvas.setBigText("This is BAD!", 2, "red",function(){
			p.playable = !p._stopOnFail;
			if(typeof p._failCallback == "function") p._failCallback();
			// restore old position of the moved cog
			p.lastMoved.moveTo(p.lastPos.x,p.lastPos.y);
			if(p.canvas.webGL instanceof JT.WebGL){
				p.canvas.webGL.setCogPos(p.lastMoved.mesh,
						p.lastPos.x*p.canvas.grid.size,
						p.lastPos.y*p.canvas.grid.size);
			}
			p.rebuildTree();
			
		},p._stopOnFail);

	};

	/** set mouse control listeners
	 */
	this._mouseHooks = function(){
		// start mouse detection
		var parent = this;
		this.canvas.addEventListener('mousemove', function(evt) {
			var position = parent.canvas.getMousePos(parent.canvas.getCanvas(), evt);
			parent.mouse.x = position.x;
			parent.mouse.y = position.y;
			// compute position on grid
			var xGrid = Math.floor(position.x/parent.canvas.grid.size);
			var xMod = position.x%parent.canvas.grid.size;
			var yGrid = Math.floor(position.y/parent.canvas.grid.size);
			var yMod = position.y%parent.canvas.grid.size;

			// find on which grid line we should move
			if(xMod > parent.canvas.grid.size/2){
				xGrid++;
			}
			if(yMod > parent.canvas.grid.size/2){
				yGrid++;
			}
			parent.mouse.gx = xGrid;
			parent.mouse.gy = yGrid;
			// writeCoordinates(parent.mouse);


			// test for mouse hovering over any cog
			if(parent._isOver(false) && parent.playable && !parent.moving){
				document.body.style.cursor = 'pointer';
			}else if ( parent.moving){
				// if we are moving some cog
				document.body.style.cursor = 'move';
			}else{ 
				// we are on empty space
				document.body.style.cursor = 'auto';
			}
		}, false);
		this.canvas.addEventListener('mousedown', function(evt) {
			parent._mouseDown();
		}, false);
		this.canvas.addEventListener('mouseup', function(evt) {
			parent._mouseUp();
		}, false);
		return this;
	};


	/** called when mouse button is pressed down 
	 */
	this._mouseDown = function(){
		// set coordinates
		this.mouse.down = true;
		this.mouse.xDown = this.mouse.x;
		this.mouse.yDown = this.mouse.y;

		// writeCoordinates(this.mouse);

		// if player was actualy moving some cog, release it
		// he probably move cursor out of canvas so it does not detect
		// releasing button
		if(this.moving){
			this.moving.show();
			this.moving.attach();
			if(this.canvas.webGL instanceof JT.WebGL){
				this.canvas.webGL.activeCog = null;
				this.canvas.webGL.activePlaceholder.visible = false;
				this.canvas.webGL.activePlaceholder = null;
			}
			this.moving = null;
		}
		// if player can play
		if(this.playable){
			
			// test if player clicked on cog
			this.moving = this._isOver(false);
			if(this.moving){
				if(this.canvas.webGL instanceof JT.WebGL){
					this.canvas.webGL.activeCog = this.moving;
					this.canvas.webGL.activePlaceholder =
						this.canvas.webGL.placeholders[this.moving.getFRadius()];
					this.canvas.webGL.activePlaceholder.visible = true;
				}
				this.moving.hide();
				this.moving.detach();
			}
		}

		return this;
	};


	/** called when mouse button is released
	 */
	this._mouseUp = function (){
		this.mouse.down = false;
		// writeCoordinates(this.mouse);
		// if player was moving some cog, release it
		if(this.moving){
			try {
				this.lastPos.x = this.moving.getPosition().x;
				this.lastPos.y = this.moving.getPosition().y;
				this.lastMoved = this.moving;
				this.moving.moveTo(this.mouse.gx,this.mouse.gy);
				if(this.canvas.webGL instanceof JT.WebGL){
					this.canvas.webGL.setCogPos(this.moving.mesh,
							this.mouse.gx*this.canvas.grid.size,
							this.mouse.gy*this.canvas.grid.size);
				}

			}catch(e){
				if(e instanceof (JT.Exception) && e.code == JT.E_CODES.NO_PLACE) {
					// do nothing, the cog will be returned

				}else{
					throw e; // another error, raise up
				}

			}
			this.moving.show();
			this.moving.attach();
			if(this.canvas.webGL instanceof JT.WebGL){
				this.canvas.webGL.activeCog = null;
				this.canvas.webGL.activePlaceholder.visible = false;
				this.canvas.webGL.activePlaceholder = null;
			}
			this.moving = null;
		}
		return this;
	};


	/** return the cog which is under mouse cursor 
	 * - or false, if nothing is there 
	 * @param {boolean} [showAll] if true is given, then return true on ANY
	 * 								cog, else only on movable cogs
	 * @returns {bool|JT.CGame.Cog}
	 */
	this._isOver = function(showAll){
		for (var i=0;i<this.allCogs.length;i++){
			if(this.allCogs[i].isMouseOver(this.mouse.x, this.mouse.y) && (showAll || this.allCogs[i].movable) ){
				return this.allCogs[i];
			}
		}
		return false;
	};
	/** **********************************************************************
	 * protected methods
	 ** **********************************************************************/
	
	/** clean game - remove all cogs all obstacles
	 * 
	 */
	this.clean = function(){

			// clean also webgl if possible
			if(this.canvas.webGL)this.canvas.webGL.clean();
			
			this.allCogs = new JT.CGame.CogsArray();
			this.movableCogs = new JT.CGame.CogsArray();
			this.poweredCog = null;
			this.rightCogs = new JT.CGame.CogsArray();
			this.badCogs = new JT.CGame.CogsArray();

			this.obstacles = new JT.CGame.ObstaclesArray();
			this.moving = null;
			this.playable = false;

	};
	
	/** set what should be done after right answer
	 * @public
	 *  
	 * @param {boolean} stop - should be player able to continue in game even after his answer?
	 * @param {function} callback 
	 */
	this.setSuccess = function(stop, callback){
		this._stopOnSuccess = stop;
		if(typeof callback != "undefined" && typeof callback != "function"&& callback != null)
			throw new JT.Exception(JT.E_CODES.BAD_PARAM,"JT.CGame.setSucces() require function or undefined or null for callback");
		this._successCallback = callback;
	};
	

	/** set what should be done after bad answer
	 * @public
	 *  
	 * @param {boolean} stop - should be player able to continue in game even after his answer?
	 * @param {function} callback 
	 */
	this.setFail = function(stop, callback){
		this._stopOnFail = stop;
		if(typeof callback != "undefined" && typeof callback != "function"&& callback != null)
			throw new JT.Exception(JT.E_CODES.BAD_PARAM,"JT.CGame.setFail() require function or undefined or null for callback");
		this._failCallback = callback;
	};
	
	/** set cog as right target 
	 * Given cog must not be in badCogs or poweredCog
	 * @public
	 * 
	 * @param {JT.CGame.Cog} cog
	 * @param {string} name - text on the cog
	 * 
	 * @return this
	 */
	this.setCogAsRight = function(cog,name){
		if(typeof cog != "object" || !(cog instanceof JT.CGame.Cog)){
			throw new JT.Exception(JT.E_CODES.BAD_PARAM,"JT.CGame.setCogAsRight() needs instance of JT.CGame.Cog as argument!");
		}
		if(cog === this.poweredCog || this.badCogs.indexOf(cog) != -1){
			throw new JT.Exception(JT.E_CODES.CANT_SET,"Cog given to JT.CGame.setCogAsRight() is already in badCogs or poweredCog!");
		}
		if( this.rightCogs.indexOf(cog) != -1){
			throw new JT.Exception(JT.E_CODES.CANT_SET,"Cog given to JT.CGame.setCogAsRight() is already set as right!");
		}
		cog.setName(name);
		cog.setNameTexture(this.canvas.createNameTexture(name,false));
		if(this.canvas.webGL) {
			this.canvas.webGL.createCogName(cog.mesh,cog.getTextures().name);
		}
		cog.movable = false;
		this.rightCogs.push(cog);

		return this;
	};


	/** set cog as bad target 
	 * Given cog must not be in rightCogs or poweredCog
	 * @public
	 * 
	 * @param {JT.CGame.Cog} cog
	 * @param {string} name - text on the cog
	 * 
	 * @return this
	 */
	this.setCogAsBad = function(cog,name){
		if(typeof cog != "object" || !(cog instanceof JT.CGame.Cog)){
			throw new JT.Exception(JT.E_CODES.BAD_PARAM,"JT.CGame.setCogAsBad() needs instance of JT.CGame.Cog as argument!");
		}
		if(cog === this.poweredCog || this.rightCogs.indexOf(cog) != -1){
			throw new JT.Exception(JT.E_CODES.CANT_SET,"Cog given to JT.CGame.setCogAsBad() is already in rightCogs or poweredCog!");
		}
		if(this.badCogs.indexOf(cog) != -1){
			throw new JT.Exception(JT.E_CODES.CANT_SET,"Cog given to JT.CGame.setCogAsBad() is already set as bad!");
		}
		cog.setName(name);
		cog.setNameTexture(this.canvas.createNameTexture(name,false));
		if(this.canvas.webGL) {
			this.canvas.webGL.createCogName(cog.mesh,cog.getTextures().name);
		}
		cog.movable = false;
		this.badCogs.push(cog);
		return this;
	};

	/** set mouse control listeners
	 * @public
	 * 
	 * 
	 */
	this.mouseHooks = function(){
		// start mouse detection
		this._mouseHooks();
		// disable text selection on canvas
		var p=this;

		document.onselectstart = function(){
			if(p.mouse.down){
				return false;
			} 
			return true;
		}; 

		return this;
	};



	/** compute distance between this and given position 
	 * @public
	 * 
	 * @param {number} x1
	 * @param {number} x2
	 * @param {number} y1
	 * @param {number} y2
	 */
	this.distance = function (x1,y1,x2,y2){
		var dx = Math.abs(x1-x2);
		var dy = Math.abs(y1-y2);
		return Math.sqrt(dx*dx + dy*dy);
	};

	/** set instance of JT.Canvas
	 * @public
	 * 
	 * @param {object} cvs
	 */
	this.setCanvas = function(cvs){
		if(typeof cvs != "object" && !(cvs instanceof JT.Canvas))
			throw new JT.Exception(JT.E_CODES.BAD_PARAM,"new JT.CGame() require JT.Canvas as parameter.");
		this.canvas = cvs;

		if(cvs.game != this) cvs.game = this;
	};


	/** Run mouse detection...
	 * @public
	 * 
	 * @return this
	 */
	this.run = function (){
		this.mouseHooks();
		this.playable = true;
	};

	
	/** create new obstacle
	 * note: Obstacles SHOULD be placed BEFORE cogs. 
	 * There is no collision checking while placing obstacles, so any obstacle 
	 * can be placed into existing cog, while cogs are checking for collision and 
	 * in an attempt to place it on obstacle raise an exception!
	 * @public
	 * 
	 * @param {number} x
	 * @param {number} y
	 * @param {number} width
	 * @param {number} height
	 * 
	 * @returns {JT.CGame.Obstacle}
	 */
	this.addObstacle = function(x,y,width,height){
		var o = new JT.CGame.Obstacle(this);
		// add webgl model if accessible
		if(this.canvas.webGL) o.mesh = this.canvas.webGL.createObstacle();
		
		o.setStart(x, y);
		o.setSize(width, height);
		o.setPadding(10);
		
		
		this.obstacles.push(o);
		
		return o;
	};

	/** Create new cog of given type and on given place
	 * note: Obstacles SHOULD be placed BEFORE cogs. 
	 * There is no collision checking while placing obstacles, so any obstacle 
	 * can be placed into existing cog, while cogs are checking for collision and 
	 * in an attempt to place it on obstacle raise an exception!
	 * @public
	 * 
	 * @param {number} x
	 * @param {number} y
	 * @param {number} size
	 * @param {number} speed        - can be only one, the root. In case of
	 *                                attempt to set speed to more cogs,
	 *                                an exception is raised
	 * @returns {JT.CGame.Cog}                               
	 */
	this.addCog = function(x,y,size,speed) {

		var cog = new JT.CGame.Cog(this,size);
		// create placeholder
		this.canvas.createPlaceholder(cog.getFRadius());
		
		
		// add webgl model if accessible
		if(this.canvas.webGL){
			// create new object
			var obj = this.canvas.webGL.createCog(
					cog.getFRadius(),
					this.canvas.getTexture(size),
					this.canvas.getHighlight(size)
				);
			cog.mesh = obj.mesh;
			this.canvas.webGL.setCogPos(cog.mesh,
					x*this.canvas.grid.size,
					y*this.canvas.grid.size);
			cog.setWebGLTexture(obj.texture);
			cog.setWebGLHighlight(obj.highlight);
			
		}
		
		// set position
		cog.moveTo(x, y);
		
		// if speed is set, this cog will be the one powered
		if(typeof speed == "number"){
			if(this.poweredCog) {
				throw new JT.Exception(JT.E_CODES.TOO_MUCH_POWERED,"Only one cog can be powered!\n"+
						"You tried to add powered cog on "+x+":"+y+
						", size "+size+", but powered cog is already"+
						"on "+this.poweredCog.getPosition().x+":"+
						this.poweredCog.getPosition().y+", size "+this.poweredCog.getGRadius()+" .");
			}
			this.poweredCog = cog;  
			cog.setSpeed(speed);
			cog.powered = true;
			cog.setNameTexture(this.canvas.createNameTexture("",true));
			if(this.canvas.webGL) {
				this.canvas.webGL.createCogName(cog.mesh,cog.getTextures().name);
			}
		}


		// set texture
		cog.setTexture(this.canvas.getTexture(size));
		cog.setHighlight(this.canvas.getHighlight(size));
		// add to list of cogs
		this.allCogs.push(cog);
		// the powered cog cannot be movable
		if(typeof speed == "undefined") {
			this.movableCogs.push(cog);
		}
		cog.attach();
		return cog;
	};

	/** rebuild the cogs tree
	 * @public
	 * 
	 */
	this.rebuildTree = function (){
		// find the root - the powered one 
		var root = null;
		for(var i=0;i<this.allCogs.length;i++){
			if(this.allCogs[i] === this.poweredCog){
				root = this.allCogs[i];
			}
		}

		var unused = this.createTree(root,this.allCogs.slice(0));

		/** check for right or bad answers:
		 * if some cog from rightCogs and badCogs is not in unused
		 * then it was already used. 
		 */
		var anythingBad = false;
		/** first check for bad answers */
		for(var i=0;i<this.badCogs.length;i++){
			if(unused.indexOf(this.badCogs[i]) == -1){
				anythingBad = true;
				this._failure();
			}
		}
		/** then for right */
		if(anythingBad==false){
			for(var i=0;i<this.rightCogs.length;i++){
				if(unused.indexOf(this.rightCogs[i]) == -1){
					this._success();
				}
			}
		}

		/* Now the tree is complete. All cogs left in unchecked
		 * are not connected to the powered one.
		 * Until there is any cog in unused, create new tree and try
		 * to 
		 */
		for(i=0; i < unused.length; i++){
			unused[i].setParent(null);
		}
		while(unused.length){
			root = unused[0];
			unused = this.createTree(root, unused);
		}
		return this;
	};



	/** This will create new tree with given root
	 * from given cogs
	 * @public
	 * 
	 * @param {JT.CGame.Cog} root
	 * @param {JT.CGame.CogsArray} available
	 * @returns {JT.CGame.CogsArray} - if any cog was not used in the new tree.
	 *                              these will be returned
	 */
	this.createTree = function(root, available){
		// add root to queue which must be checked
		var queue = new Array(root);

		// all others cogs to another queue
		//var unchecked = this.allCogs.slice(0);

		available.splice(available.indexOf(root),1);


		var completed = new Array();
		/* until queue is empty, search for connected cogs
		 * and once the cog is finished, add it to completed */
		while(queue.length > 0){
			// remove this cog from queue
			var item = queue.shift();
			// check all available cogs 
			for(var i=0;i<available.length;i++){
				var cog = available[i];
				// if these cogs are next to this one
				// if yes, add it to queue and set parent to this
				if(item.isNext(cog)){
					queue.push(cog);
					available.splice(available.indexOf(cog),1);
					cog.setParent(item);
					i--; // because the array is now smaller
				}
			}

			// place to completed
			completed.push(item);
		}
		// if any cogs left, return them
		return available;
	};



	/** Check if on given position is enough space for cog of given size
	 * Return true if there is space, false if not.
	 * @public
	 * 
	 * @param {JT.CGame.Cog}
	 * @param {number} x
	 * @param {number} y
	 * 
	 * @returns {bool}
	 */
	this.testSpace = function (cog, x,y){
		// Distance of two points in 2d space.

		/** check for every cog */
		for(var i = 0; i<this.allCogs.length;i++){
			// if we are on this cog, skip it
			if(this.allCogs[i] === cog) continue;

			var d = this.distance(x,y,this.allCogs[i].getPosition().x,this.allCogs[i].getPosition().y);

			// if distance is smaller then halfdiameters of both cogs
			// return false
			if(d <  (this.allCogs[i].getGRadius()+cog.getGRadius())) {
				//console.log(x+":"+y+" vs "+this.allCogs[i].getPosition().x+":"+this.allCogs[i].getPosition().y);
				return false;
			}else if (d ==  (this.allCogs[i].getGRadius()+cog.getGRadius())){

				// check if it is not in 90 deg. angle 
				if(x != this.allCogs[i].getPosition().x && y != this.allCogs[i].getPosition().y){
					
					return false;
				}
			}
		}
		
		/**
		 * check for obstacles
		 */
		for(var i=0; i< this.obstacles.length;i++){
			if(this.obstacles[i].testCollision(x,y,cog.getGRadius())){
				return false;
				
			}
		}
		
		
		/** if no collision was found, return true */
		return true;
	};


	/** **********************************************************************
	 * Public methods
	 ** **********************************************************************/
	//JT.CGame.Cog.prototype.

	/** **********************************************************************
	 * constructor
	 ** **********************************************************************/

	return this;
};
JT.CGame.inheritsFrom(JT.Class);
/** **********************************************************************
 * constant properties and methods
 ** **********************************************************************/



/** special type
 * @type Array
 */
JT.CGame.CogsArray = JT.CGame.CogsArray || Array;
JT.CGame.ObstaclesArray = JT.CGame.ObstaclesArray || Array;


