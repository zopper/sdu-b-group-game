<!DOCTYPE HTML>
<html>
<head>
<title>Canvas test</title>

<script src="js/start-game.js"></script>
<script>
	// use "i" for access to iframe canvasWrapper in console
	// use "j" for access to iframe webglWrapper in console
	// eg. i.JT.$()
	var i = null;
	var j = null;
	window.onload = function() {
		i = document.getElementById("canvasWrapper").contentWindow;
		j = document.getElementById("webglWrapper").contentWindow;
		a=startGame(i,"2d","questionI");
		b=startGame(j,"webgl","questionJ");
	};
</script>

<style>
body {
	background-color: #cacaca;
}

#canvasWrapper {
	width: 640px;
	height: 512px;
	border: 1px solid #9C9898;
	background-color: white;
	position: relative;
}

#webglWrapper {
	width: 640px;
	height: 512px;
	border: 1px solid #9C9898;
	background-color: white;
	position: relative;
}

.question {
	background-color: #dadaaa;
	border: 1px solid #992;
	padding: 5px;
}

table {
	padding:5px;
}

#howto {
	background-color: #eee;
	border: 1px solid #999;
	padding: 5px;
}

</style>

</head>
<body>
	<h1 id="header">Demo</h1>
	<p style="font-size: 70%">NOTE: Actualy there are only 3 levels,
		randomly selected. For access to the iframe in console use variable
		"i" for left and "j" for right.</p>
	<p style="font-size: 70%">NOTE2: On this page are both canvas 2d and webgl engines for comparision.
	Both should act equal (each of them selects level independently).</p>
	
	<div id="howto"><b>how to play</b> <br>
	 <ul><li>Read question, find the cog with the same letter as answer to the question and try to connect this one cog with the turning cog (with yellow center) by draging other cogs.<br>
	<li>The powered one cog and these with answers cannot be moved.
	<li>For better experience open "only canvas 2D" or "only webGL" page - this dualmode has smaller screen (and lower FPS)
	</ul>
	</div>

	<table>
		<tr>
			<th>Canvas 2D - <a href="index2d.php">Only canvas 2D</a></th>
			<th>WebGL Three.js - <a href="indexWebGL.php">Only webGL</a>
				<br> WARNING: Internet Explorer does not support WebGL and opera has some trouble with three.js implementation!
				</th>
		</tr>
		<tr>
			<th id="questionI" class="question"></th>
			<th id="questionJ" class="question"></th>
		</tr>
		<tr>
			<td><iframe id="canvasWrapper" src="game.php"> </iframe>
			</td>
			<td><iframe id="webglWrapper" src="game.php"></iframe>
			</td>
		</tr>


	</table>

	<!-- -->

	<!--
	<div id="canvasWrapper">
	</div>
	<!-- -->

</body>
</html>
