<!DOCTYPE HTML>
<html>
<head>
<title>Canvas</title>

<script src="libs/three.js"></script>
<script src="libs/stats.js"></script>
<script src="js/JT.js"></script>
<script src="js/JT.Exception.js"></script>
<script src="js/JT.CGame.js"></script>
<script src="js/JT.Canvas.js"></script>
<script src="js/JT.CGame.Canvas.js"></script>
<script src="js/JT.WebGL.js"></script>
<script src="js/JT.CGame.Cog.js"></script>
<script src="js/JT.CGame.Obstacle.js"></script>

<style>
body {
	padding: 0;
	margin: 0;
	background-color: white;
	position: relative;
}
canvas {
	position: absolute;
	top:0;
	left:0;
	padding:0;
}
</style>

</head>
<body>

</body>
</html>
